#!/opt/anaconda-3/anaconda3/bin/python3
# -*- coding: utf-8 -*-

import os
from time import time
import sys
import re
import numpy as np
import pandas as pd
import pickle
from sklearn.externals import joblib
from sklearn.ensemble import ExtraTreesRegressor, RandomForestRegressor
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA

import matplotlib as mpl 
mpl.use("agg")
#mpl.use('Qt5Agg')
from matplotlib import pyplot as plt 
mpl.rc('font', family='serif') 
mpl.rc('text', usetex='true') 
mpl.rc('text', dvipnghack='true') 
mpl.rcParams.update({'font.size': 8}) 
import pylab as P
from sys import argv
import corner
import tensorflow as tf
import argparse
import subprocess as sp
import multiprocessing as mp
from tensorflow.contrib.learn.python import SKCompat
from sklearn.metrics import explained_variance_score
from scipy import stats
from scipy.stats import norm
from prettytable import PrettyTable

import lim_perturb as lp
import RF.param_read as pr
from RF.lim_pertfreqs import  lin_reg

"""Random Forest Regressor to hanlde RGB Grid and Tensor Flow capabilities. 
   Tensor Flow performs very poorly compared to Scikit Learn so I stopped 
   developing it for now. Most of this is based on the orginal code by 
   Earl Bellinger. """    



### Labels and some definitions 
y_latex = {
    "M": r"Mass M$/$M$_\odot$", 
    "Y": r"Initial helium Y$_0$", 
    "Z": r"Initial metallicity Z$_0$", 
    "alpha": r"Mixing length $\alpha_{\mathrm{MLT}}$", 
    "diffusion": r"Diffusion factor D",
    "overshoot": r"Overshoot $\alpha_{\mathrm{ov}}$", 
    "age": r"Age $\tau/$Gyr", 
    "radius": r"Radius R$/$R$_\odot$", 
    "mass_X": r"Hydrogen mass X", 
    "mass_Y": r"Helium mass Y",
    "X_surf": r"Surface hydrogen X$_{\mathrm{surf}}$", 
    "Y_surf": r"Surface helium Y$_{\mathrm{surf}}$",
    "X_c": r"Core-hydrogen X$_{\mathrm{c}}$",
    "log_g": r"Surface gravity log g (cgs)", 
    "L": r"Luminosity L$/$L$_\odot$",
    "mass_cc": r"Convective-core mass M$_{\mathrm{cc}}$"
}

y_latex_short = {
    "M": r"M$/$M$_\odot$", 
    "Y": r"Y$_0$", 
    "Z": r"Z$_0$", 
    "alpha": r"$\alpha_{\mathrm{MLT}}$", 
    "diffusion": r"D",
    "overshoot": r"$\alpha_{\mathrm{ov}}$",
    "age": r"$\tau/$Gyr", 
    "radius": r"R$/$R$_\odot$", 
    "mass_X": r"X", 
    "mass_Y": r"Y",
    "X_surf": r"X$_{\mathrm{surf}}$", 
    "Y_surf": r"Y$_{\mathrm{surf}}$",
    "X_c": r"X$_{\mathrm{c}}$",
    "log_g": r"log g", 
    "L": r"L$/$L$_\odot$",
    "mass_cc": r"M$_{\mathrm{cc}}$"
}




################################################################################
### Classes ####################################################################
################################################################################
class sk_rf(object):
   """SKlearn random forest algorithm. This is where the magic happens""" 
   def __init__(self):
      self.name ='SKlearn Random Forest Regressor'
      self.sn='sk'
      self.forest=None
      self.forests=[]
   def train_regressor(self,data, X_columns, Y_columns, params,default=False):
       
       
       
       if default is False:
          ### We should not use slopes and intercepts from subgiants. It gets Yukky
          if params.drop_intercepts is False and any( x in list(data) for x in  ['r02c','r01c','r10c','r13c'] ):
             print('We want to use slopes and intercepts of ratios as a feature and they are still in training data')
             print('Not reliable beyond MS. Dropping everything beyond CHE')
             print('Training data shape:', data.shape)
             idx=np.where(data['ev_stage'] > 2)[0]
             data=data.drop(idx)
             print('Shape after dropping:', data.shape)
       
          ### Get the Columns
          y_show=[y for y in Y_columns if y not in params.ex]          
          
       else:
          y_show=Y_columns 
          
          
       ys = data.loc[:, y_show]
       X = data.loc[:,[i for i in X_columns if i not in y_show and i in list(data)]]


       ### Now need to drop rows where there is NAN. 
       idx=X.isnull().any(axis=1)  
       kdx=[j for j in X.index if idx.loc[j]  == True]
       idx=ys.isnull().any(axis=1)    
       kdx+= [j for j in ys.index if idx.loc[j]  == True]  
       kdx=list(set(kdx))
       print('Dropping due to nan', len(kdx))
       #print(kdx[:20])
       X=X.drop(kdx)
       ys=ys.drop(kdx)
        
       ### Actually Train
       print()
       print('Training', self.name)
       print('Training Grid', X.shape)
       for n_trees in [params.ntrees]:
         self.forest = Pipeline(steps=[
            ('forest', ExtraTreesRegressor(
                #RandomForestRegressor(
                n_estimators=n_trees, 
                n_jobs=min(n_trees, mp.cpu_count()-2),
                oob_score=True, bootstrap=True))])
         start = time()
   
         ### Did any null values slip through 
         if X.isnull().any().any():
            print('null values in array')
            print(X.isnull().any()) 

            

         self.forest.fit(X, ys)#new_ys)
         end = time()
         print('ntree', 'oob', 'time')
         print(n_trees, self.forest.steps[0][1].oob_score_, end-start)
    
       print()
       print("%.5g seconds to train regressor" % (end-start))
       print()
    
       self.y_names = ys.columns
       self.X_names = X.columns
       
       
       
   def save_forest(self):
        """Store to see if we have already trained an appropriate forest"""
        ### Not the most sophticated way of doing this
        self.forests.append((self.forest,self.X_names,self.y_names))      
       
class tf_rf(object):
  """Class to use google's tensor flow machine learning libraries. Their random forest implementation is poor.
     It requires a lot of resources and does not perform nearly as well as scikit learn. 
     So I have stopped developing this class"""
    
  def __init__(self):
      self.name ='TensorFlow Random Forest Regressor'
      self.sn='tf'
      self.forest=None
      self.forests=[]
  def train_regressor(self,data, X_columns, params):
      
       y_show=params.y_init+ params.y_curr 
       X = data.loc[:,X_columns]
       ys = data.loc[:, [i for i in y_show if i not in X_columns]]
    
       print()
       print('Training', self.name)
 
       params = tf.contrib.tensor_forest.python.tensor_forest.ForestHParams(
                 num_classes=len(list(ys)), 
                 num_features=len(list(X)),
                 num_trees=100,
                 bagging_fraction=1.0,
                 feature_bagging_fraction=0.9,
                 regression=True).fill()


       estimator = SKCompat(tf.contrib.tensor_forest.client.random_forest.TensorForestEstimator(
                   params, model_dir="./tfout"))
       start = time()  
       if X.isnull().any().any():
            print('null values in array')
            print(X.isnull().any())  

       self.forest=estimator.fit(x=np.asarray(X).astype(np.float32), y=np.asarray(ys).astype(np.float32))
       end = time()

    
       print()
       print("%.5g seconds to train regressor" % (end-start))
       print()
       self.y_names = ys.columns
       self.X_names = X.columns

  def save_forest(self):
     ### Not the most sophticated way of doing this
     self.forests.append((self.forest,self.X_names,self.y_names))    




################################################################################
### Helpers ####################################################################
################################################################################
def get_rc(num_ys):
    rows = (num_ys+(1 if num_ys%2 else 0)) // 2
    cols = 4 if num_ys%2 else 2
    if num_ys%3==0:
        rows = num_ys//3
        cols = num_ys//rows
    sqrt = np.sqrt(num_ys)
    if int(sqrt) == sqrt:
        rows, cols = int(sqrt), int(sqrt)
    return rows, cols, sqrt

def weighted_avg_and_std(values, weights):
    average = np.average(values, axis=0, weights=weights)
    variance = np.average((values-average)**2, axis=0, weights=weights) 
    return (average, np.sqrt(variance))

def gumr(xn, xu):
    z2 = np.trunc(np.log10(xu))+1
    z1 = np.around(xu/(10**z2), 3)
    y1 = np.around(xn*10**(-z2), 2)
    value = y1*10**z2
    uncert = z1*10**z2
    return('%g'%value, '%g'%uncert)

def print_star(star, predict, y_names, table_curr, table_init,params):
    middles = np.mean(predict, 0)
    stds = np.std(predict, 0)
    #middles, stds = weighted_avg_and_std(predict, 1/stds)
    outstr = star
    initstr = "\n" + star
    currstr = "\n" + star
    for (pred_j, name) in enumerate(y_names):
        (m, s) = (middles[pred_j], stds[pred_j])
        m, s = gumr(m, s)
        outstr += "\t%s pm %s" % (m, s)
        if name in params.y_init:
            initstr += r" & %s $\pm$ %s" % (m, s)
        if name in params.y_curr:
            currstr += r" & %s $\pm$ %s" % (m, s)
    print(outstr)
    table_curr.write(currstr + r' \\')
    table_init.write(initstr + r' \\')

def print_star2(star, predict, y_names, table_curr, table_init,params):
    middles = np.mean(predict, 0)
    stds = np.std(predict, 0)
    #middles, stds = weighted_avg_and_std(predict, 1/stds)
    outstr = star
    initstr = "\n" + star
    currstr = "\n" + star
    x = PrettyTable()
    cols=['Star']
    vals=[star]
    for (pred_j, name) in enumerate(y_names):
        (m, s) = (middles[pred_j], stds[pred_j])
        m, s = gumr(m, s)
        outstr = "%s pm %s" % (m, s)
        cols.append(name)
        vals.append(outstr)
        if name in params.y_init:
            initstr += r" & %s $\pm$ %s" % (m, s)
        if name in params.y_curr:
            currstr += r" & %s $\pm$ %s" % (m, s)
    x.field_names=cols
    x.add_row(vals) 
    print(x)
    table_curr.write(currstr + r' \\')
    table_init.write(initstr + r' \\')


def write_table_headers(y_names, table_curr, table_init,params):
    #print("Name\t"+"\t".join(y_names))
    table_curr.write(
        r"\tablehead{\colhead{Name} & "+\
        ' & '.join([r"\colhead{" + y_latex_short[yy] + r"}"
                    for yy in y_names if yy in params.y_curr]) +\
        r"}\startdata" )
    table_init.write(
        r"\tablehead{\colhead{Name} & "+\
        ' & '.join([r"\colhead{" + y_latex_short[yy] + r"}"
                    for yy in y_names if yy in params.y_init]) +\
        r"}\startdata" )
    
def plot_line(value, n, bins, plt, style):
    cands = bins > value
    if any(cands):
        height = n[np.where(bins > value)[0][0]-1]
    else:
        height = n[0]
    plt.plot((value, value), (0, height), style)

def plot_star(star, predict, y_names, out_dir, sn):
    
    ## Regular histograms
    middles = np.mean(predict, 0)
    stds = np.std(predict, 0)
    
    #outstr = star
    num_ys = predict.shape[1]
    
    rows, cols, sqrt = get_rc(num_ys)
    
    
    ### Try Preacclocation to stop mapliotlib 2.2.2 crash
    if num_ys%2==0 or num_ys%3==0 or int(sqrt)==sqrt:  
      fig,axs=plt.subplots(rows,cols,figsize=(6.97522*2, 4.17309*2), dpi=400, 
        facecolor='w', edgecolor='k', linewidth=1.)   
        
    else:    
      plt.figure(figsize=(6.97522*2, 4.17309*2), dpi=400, 
        facecolor='w', edgecolor='k', linewidth=1.)
    for (pred_j, name) in enumerate(y_names[0:num_ys]):
        (m, s) = (middles[pred_j], stds[pred_j])
        
        if num_ys%2==0 or num_ys%3==0 or int(sqrt)==sqrt:  
            ax = axs.flat[pred_j]
        elif pred_j%2==0 and pred_j == num_ys-1:
            ax = plt.subplot2grid((rows, cols), (pred_j//2, 1), colspan=2)
        else:
            ax = plt.subplot2grid((rows, cols), (pred_j//2, (pred_j%2)*2),
                colspan=2)
        
        n, bins, patches = ax.hist(predict[:,pred_j], 50, density=1, 
            histtype='stepfilled', color='white',edgecolor='black', linewidth=1.)
        
        #if y_central is not None:
        #    mean = np.mean(y_central[:,pred_j])
        #    std = np.std(y_central[:,pred_j])
        #    
        #    plot_line(mean, n, bins, plt, 'r--')
        #    plot_line(mean+std, n, bins, plt, 'r-.')
        #    plot_line(mean-std, n, bins, plt, 'r-.')
        
        q_16, q_50, q_84 = corner.quantile(predict[:,pred_j], [0.16, 0.5, 0.84])
        q_m, q_p = q_50-q_16, q_84-q_50
        plot_line(q_50, n, bins, ax, 'k--')
        plot_line(q_16, n, bins, ax, 'k-.')
        plot_line(q_84, n, bins, ax, 'k-.')
        
        # Format the quantile display.
        fmt = "{{0:{0}}}".format(".3g").format
        title = r"${{{0}}}_{{-{1}}}^{{+{2}}}$"
        title = title.format(fmt(q_50), fmt(q_m), fmt(q_p))
        
        ax.annotate(r"$\epsilon = %.3g\%%$" % (s/m*100),
            xy=(0.99, 0.12), xycoords='axes fraction',
            horizontalalignment='right', verticalalignment='right')
        
        ax.set_xlabel(y_latex[y_names[pred_j]] + " = " + title)
        ax.locator_params(axis='x', nbins=3)
        
        xs = [max(0, m-4*s), m+4*s]
        
        xticks = [max(0, m-3*s), m, m+3*s]
        ax.set_xticks(xticks)
        ax.set_xlim(xs)
        ax.set_xticklabels(['%.3g'%xtick for xtick in xticks])
        ax.set_yticklabels('',visible=False)
        
        ax.set_frame_on(False)
        ax.get_xaxis().tick_bottom()
        ax.axes.get_yaxis().set_visible(False)
        
        #xmin, xmax = ax1.get_xaxis().get_view_interval()
        ymin, ymax = ax.get_yaxis().get_view_interval()
        ax.add_artist(mpl.lines.Line2D(xs, (ymin, ymin), 
            color='black', linewidth=2))
        
        #ax.minorticks_on()
        plt.tight_layout()

    plt.savefig(os.path.join(out_dir, star +'_'+sn+ '.pdf'), dpi=400)
    plt.close()



def plot_star_old(star, predict, y_names, out_dir, sn):
    
    ## Regular histograms
    middles = np.mean(predict, 0)
    stds = np.std(predict, 0)
    
    #outstr = star
    num_ys = predict.shape[1]
    
    rows, cols, sqrt = get_rc(num_ys)
    plt.figure(figsize=(6.97522*2, 4.17309*2), dpi=400, 
        facecolor='w', edgecolor='k', linewidth=1.)
    for (pred_j, name) in enumerate(y_names[0:num_ys]):
        (m, s) = (middles[pred_j], stds[pred_j])
        
        if num_ys%2==0 or num_ys%3==0 or int(sqrt)==sqrt:  
            ax = plt.subplot(rows, cols, pred_j+1)
        elif pred_j%2==0 and pred_j == num_ys-1:
            ax = plt.subplot2grid((rows, cols), (pred_j//2, 1), colspan=2)
        else:
            ax = plt.subplot2grid((rows, cols), (pred_j//2, (pred_j%2)*2),
                colspan=2)
        
        n, bins, patches = ax.hist(predict[:,pred_j], 50, density=1, 
            histtype='stepfilled', color='white',edgecolor='black', linewidth=1.)
        
        #if y_central is not None:
        #    mean = np.mean(y_central[:,pred_j])
        #    std = np.std(y_central[:,pred_j])
        #    
        #    plot_line(mean, n, bins, plt, 'r--')
        #    plot_line(mean+std, n, bins, plt, 'r-.')
        #    plot_line(mean-std, n, bins, plt, 'r-.')
        
        q_16, q_50, q_84 = corner.quantile(predict[:,pred_j], [0.16, 0.5, 0.84])
        q_m, q_p = q_50-q_16, q_84-q_50
        plot_line(q_50, n, bins, plt, 'k--')
        plot_line(q_16, n, bins, plt, 'k-.')
        plot_line(q_84, n, bins, plt, 'k-.')
        
        # Format the quantile display.
        fmt = "{{0:{0}}}".format(".3g").format
        title = r"${{{0}}}_{{-{1}}}^{{+{2}}}$"
        title = title.format(fmt(q_50), fmt(q_m), fmt(q_p))
        
        ax.annotate(r"$\epsilon = %.3g\%%$" % (s/m*100),
            xy=(0.99, 0.12), xycoords='axes fraction',
            horizontalalignment='right', verticalalignment='right')
        
        P.xlabel(y_latex[y_names[pred_j]] + " = " + title)
        P.locator_params(axis='x', nbins=3)
        
        xs = [max(0, m-4*s), m+4*s]
        
        xticks = [max(0, m-3*s), m, m+3*s]
        ax.set_xticks(xticks)
        ax.set_xlim(xs)
        ax.set_xticklabels(['%.3g'%xtick for xtick in xticks])
        ax.set_yticklabels('',visible=False)
        
        ax.set_frame_on(False)
        ax.get_xaxis().tick_bottom()
        ax.axes.get_yaxis().set_visible(False)
        
        #xmin, xmax = ax1.get_xaxis().get_view_interval()
        ymin, ymax = ax.get_yaxis().get_view_interval()
        ax.add_artist(mpl.lines.Line2D(xs, (ymin, ymin), 
            color='black', linewidth=2))
        
        #ax.minorticks_on()
        plt.tight_layout()
    

    plt.savefig(os.path.join(out_dir, star +'_'+sn+ '.pdf'), dpi=400)
    plt.close()



def plot_importances(forest, star, baseout, X_names):
    """Work out the feature importances for the RF"""
    est = forest.steps[0][1]
    importances = est.feature_importances_
    indices = np.argsort(importances)[::-1]
    import_dist = np.array([tree.feature_importances_ 
        for tree in est.estimators_])
    
    cpath=os.path.join(baseout,'covs','feature-importance-'+star+'.dat')
    if not os.path.exists(os.path.dirname(cpath)):
        os.makedirs(os.path.dirname(cpath))
    
    np.savetxt(cpath,
        import_dist, header=" ".join(X_names), comments='')
    
    import_dist = import_dist.T[indices][::-1].T
    
    print("Feature ranking:")
    for f in range(len(X_names)):
        print("%d. %s (%.3g)" % (f+1, X_names[indices[f]], 
            importances[indices[f]]))
    print()
    
    mpl.rc('text', usetex='false') 
    plt.figure(figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
    plt.boxplot(import_dist, vert=0)
    plt.yticks(range(1,1+len(X_names)),
               np.array(X_names)[indices][::-1])
    plt.xlabel("Feature importance")
    plt.tight_layout()
    cpath=os.path.join(baseout,'plots','feature-importance-'+star+'.pdf')
    if not os.path.exists(os.path.dirname(cpath)):
        os.makedirs(os.path.dirname(cpath))
    plt.savefig(cpath)
    plt.close()

    t1=['Teff','r10','r01','L','Dnu0','diffusion','Fe_H','radius','dnu02','r02']
    mlist=[]
    for val in t1:
        if val in X_names:
          idx=np.where(X_names==val)[0][0] 
          msg=' & {0:.2f}'.format(importances[idx])    
        else: 
          msg =' &'   
        mlist.append(msg)         
    out="".join(mlist)
    print(out)
    print(t1)


def paper_out(star, star_data, predict, paper_table, params,ynames):
    
   """Print a table. Its format will depend on what I want to include for the paper """ 
   
   GS98logZX=np.log10(0.0230)
   t1=['Dnu0', 'Teff', 'Fe_H', 'radius', 'L', 'alpha', 'overshoot']
   t2=['M','age','X_c', 'log_g','radius', 'L', 'alpha', 'overshoot', 'Teff'] 
   
   #t2=['M','radius','age','X_c', 'log_g', 'L', 'alpha', 'overshoot','undershoot', 'mass_cc', 'cz_radius','FE_H_surf'] 
   t2=['M','radius','age','Teff','L', 'FE_H_out','alpha', 'overshoot']
   a=os.path.dirname(paper_table)
   a=os.path.dirname(a)
   obs=a.replace(params.out_dir, params.obs_dir)
   if obs.endswith('_DF'):
       obs=obs[:-3]
   fobs=[os.path.join(obs,x) for x in os.listdir(obs) if x.endswith(params.obs_pattern)] 

   for star in fobs:
      mlist=[]
      obs_data= pd.read_table(star,delim_whitespace=True)
      obs_data.columns=['Name','Value','Uncertainty']   
      obs_data.index=obs_data.Name 
      if 'Dnu0' in list(star_data):             
         obs_data.loc['Dnu0']=('Dnu0', star_data.Dnu0.mean(), star_data.Dnu0.std())
      for val in t1:
         if val in obs_data.index.values:
           if val=='Teff':
             msg=' & ${0:d} \pm {1:d}$'.format(int(obs_data.loc[val]['Value']),int(obs_data.loc[val]['Uncertainty']))   
           else: 
              msg=' & $ {0:.2f} \pm {1:.2f}$'.format(obs_data.loc[val]['Value'],obs_data.loc[val]['Uncertainty'])   
         
         else:
           msg =' &'  
         mlist.append(msg) 
      out="".join(mlist)
 
      print() 
      print (out)
      print()

      mlist2=[]
      middles = np.mean(predict, 0)
      stds = np.std(predict, 0)
      for val in t2:
        if val in ynames:
          idx=np.where(ynames==val)[0][0]
          msg=' & ${0:.2f} \pm {1:.2f}$'.format(middles[idx],stds[idx]) 
        elif val == 'FE_H_surf':
            idx1=np.where(ynames=='X_surf')[0][0]
            idx2=np.where(ynames=='Y_surf')[0][0] 
            Fehs= np.log10( (1.0-predict[:,idx1]-predict[:,idx2])/predict[:,idx1]) - GS98logZX
            msg=' & ${0:.2f} \pm {1:.2f}$'.format(Fehs.mean(),Fehs.std())    
   
        else: 
          msg =' &'   
        mlist2.append(msg)
      out2="".join(mlist2)
     
      print(out2)
      with open(paper_table, 'w') as wf:
        wf.writelines(out + '\n') 
        wf.writelines(out2 + '\n')


def check_mon(x,y):
   ds=np.sign(np.diff(y))
   if len(ds) > 2:
      if ds[0] != ds[2] and ds[1] !=ds[2]:
          x=x[2:]
          y=y[2:] 
          return x,y  
      if ds[0] != ds[1] :
          x=x[1:]
          y=y[1:]  

      signs=[idx for idx, i in enumerate((np.diff(np.sign(y)) != 0)*1) if i==1]
      if 0 in signs and 1 in signs and 2 in signs:
        x=x[3:]
        y=y[3:]
        return x,y
      if 0 in signs and 1 in signs:
        x=x[2:]
        y=y[2:]
        return x,y
      if 0 in signs:   
        x=x[1:]
        y=y[1:]

   return x,y



def calc_inct(data, fcols):
    """calculate the slope and intercept of the ratios"""
    
    ratios=['r01','r10','r02','r13']
    drats={}

    for rat in ratios:
      drats[rat+'m'] = []
      drats[rat+'c'] = []

    ### Get the column of each radial order ratio
    ir01=[i for i,rat in enumerate(fcols) if 'r01_' in rat]
    ir10=[i for i,rat in enumerate(fcols) if 'r10_' in rat]
    ir02=[i for i,rat in enumerate(fcols) if 'r02_' in rat]
    ir13=[i for i,rat in enumerate(fcols) if 'r13_' in rat]

    idxs=[ir01,ir10,ir02,ir13]
    

    for row in data.itertuples(index=True, name='Pandas'):
       for j, idx in enumerate(idxs):
         x=[int(fcols[k].split('_')[1]) for k in idx if not pd.isnull(row[k+1])]
         y=[row[k+1] for k in idx  if not pd.isnull(row[k+1])]
         ### Sort and check for montonicity
         idx=np.argsort(np.asarray(x))
         x=[x[i] for i in idx]
         y=[y[i] for i in idx]
         x,y=check_mon(x,y)
                  
         slope, intercept = lin_reg(x,y)
         drats[ratios[j]+'m'].append(slope)
         drats[ratios[j]+'c'].append(intercept)

    for key, item in drats.items():
       data[key]=item

    #print('calc_intc', list(data))    
    return data


def calc_inct2(data, fcols):
    """calculate the slope and intercept of the ratios"""
    
    ratios=['r01','r10','r02','r13']
    drats={}

    for rat in ratios:
      drats[rat+'m'] = []
      drats[rat+'c'] = []

    ### Get the column of each radial order ratio
    ir01=[i for i,rat in enumerate(fcols) if 'r01_' in rat]
    ir10=[i for i,rat in enumerate(fcols) if 'r10_' in rat]
    ir02=[i for i,rat in enumerate(fcols) if 'r02_' in rat]
    ir13=[i for i,rat in enumerate(fcols) if 'r13_' in rat]

    idxs=[ir01,ir10,ir02,ir13]
       
    idnu=fcols.index('Dnu0')
    inm=fcols.index('nu_max')
    empty=[]
    for row in data.itertuples(index=True, name='Pandas'):
       for j, idx in enumerate(idxs):
         x=[int(fcols[k].split('_')[1]) for k in idx if not pd.isnull(row[k+1])]
         y=[row[k+1] for k in idx  if not pd.isnull(row[k+1])]
         ### Sort and check for montonicity
         idx=np.argsort(np.asarray(x))
         x=[x[i] for i in idx]
         y=[y[i] for i in idx]
         
         if len(x) > 3:
           nidx=row[inm+1]//row[idnu+1]
           pos = (np.abs(np.asarray(x)-nidx)).argmin()         
           wts = norm.pdf(x, loc=x[pos], scale=2.5)
           slope, intercept = lin_reg(x,y,wts)
         else:
           empty.append(row[0])
           slope, intercept =np.nan, np.nan
         
         drats[ratios[j]+'m'].append(slope)
         drats[ratios[j]+'c'].append(intercept)

    for key, item in drats.items():
       data[key]=item

    #print('calc_intc2 empties', empty)    
    return data

def load_default_forest(params,data):
    """Here we have a default forest to help deal with subgiants and ratios/intercepts""" 
    default_forest_loc={'sk':'./RF/grid_lims/default_forest_sk.pickle', 
                        'tf':'./RF/grid_lims/default_forest_tf.pickle'}
    
    Xcols=['Teff','Dnu0','Fe_H']
    Ycols=['M','age','radius','alpha','ev_stage']
    for reg in regs:
       print('Training new default forest') 
       reg.train_regressor(data, Xcols,Ycols,params,default=True)
       reg.save_forest()
       
    ### Turns out its quicker to retrain the forest than dump and reload
    """
    for reg in regs:
        if os.path.isfile(default_forest_loc[reg.sn]):
           print('Loading Deafult Forest') 
           start = time()
           reg.forest= joblib.load(default_forest_loc[reg.sn])
           end=time()
           print("%.5g seconds to load pickled default forest" % (end-start))
           reg.X_names=Xcols
           reg.y_names=Ycols
           reg.save_forest()
        else:
            print('Training and saving new default forest') 
            reg.train_regressor(data, Xcols,Ycols,params,default=True)
            reg.save_forest()
            joblib.dump(reg.forest, default_forest_loc[reg.sn])"""
           
def regress_star(star, star_data, Ycols, data,params,baseout):  

       ### Send to appropriate regressor library. Do some intialization. 
       for reg in regs:       
         table_curr_fname = os.path.join(baseout, 'tables', star+"_curr_"+reg.sn+".dat")
         table_init_fname = os.path.join(baseout, 'tables', star+"_init_"+reg.sn+".dat")
         if not os.path.exists(os.path.dirname(table_curr_fname)):
            os.makedirs(os.path.dirname(table_curr_fname))
         table_curr = open(table_curr_fname, 'w')
         table_init = open(table_init_fname, 'w')   
       
         wrong_cols = []
         outside = []
         run_times = []
                  
         ### Check we don't already have an appropriate forest. Gets tricky now with slopes and intercepts
         Train = True
         if len(reg.forests) >  0:
             for fst in reg.forests:
                 sdc=[x for x in star_data.columns if x not in Ycols]
                 if set(sdc) ==set(fst[1]):
                     
                   ### Always retrain if we calculate slopes and intercepts from specified/limited modes  
                   if params.train_int_slope is True and params.train_int_slope_all==1:  
                     print('We are training on slopes and intercepts. Furthermore, train_int_slope_all=1.')
                     print('This means we redo the linear regression based on the availible modes of each star')
                     print('Not worth checking this. Retraining instead')
                     Train=True
                     
                   ### If we are using all slopes and intercepts, we comapre and hope for the best. 
                   ### If we dont use slopes/intercepts can also use appropriate forest.    
                   elif (params.train_int_slope is True and params.train_int_slope_all==0) or  params.train_int_slope is False: 
                     print('Already trained an appropriate forest, Loading')
                     reg.forest=fst[0]
                     reg.X_names=fst[1]
                     reg.y_names=fst[2]
                     Train=False
                     
         ### Train Regressor if need be  
         if reg.forest is None or Train is True :
            print('Training New RF for ',star) 
            X_columns = star_data.columns    
            reg.train_regressor(data, X_columns,Ycols,params)
            reg.save_forest()

         ### Assign local variables
         forest=reg.forest
         X_names=reg.X_names
         y_names=reg.y_names

         plot_importances(forest, star+'-'+reg.sn, baseout, X_names)
         write_table_headers(y_names, table_curr, table_init, params)


         ### Redundancy 1: check that it has all of the right columns
         if not set(X_names).issubset(set(star_data.columns)):
            wrong_cols += [star]
            continue
         
         ### Redundancy 2: check if all the params are within the grid
         ### Probably shouldnt happen.
         out_of_range = False
         
         idx=[]
         oname=[]
         for X_name in set(X_names):
            upper = params.maxs[X_name]
            lower = params.mins[X_name]
            X_vals = star_data.loc[:, X_name]
            if np.any(X_vals > upper) or np.any(X_vals < lower):
               
               t1=list(np.where(X_vals > upper)[0])
               t2=list(np.where(X_vals < lower)[0])
               oname+= [(X_name, 'upper')]*len(t1)
               oname+= [(X_name, 'lower')]*len(t1)
               #print(X_name, t1, t2) 
               idx += t1+t2
               
         ### Can drop a few instantiations if outside the grid params:
         if params.max_outside > 0:
            if len(idx) < params.max_outside:             
              star_data=star_data.drop(idx)
            else:
             print('Too many instantiations outside grid range')
             #print(oname)
             #for kk in range(len(idx)):
             #   print(oname[kk], idx[kk])
             break   
             quit()
         else:
           star_data=star_data.drop(idx)
           
         
         ### Send star data to Random Forest
         star_X = star_data.loc[:,X_names]
         start = time()
         predict = forest.predict(star_X)

         ### Save and Print Results
         end = time()
         run_times += [end-start]


         cov_subdir=os.path.join(baseout, 'covs', star+'_'+reg.sn+'.dat')
         out_dir=os.path.join(baseout, 'plots') 
         if not os.path.exists(os.path.dirname(cov_subdir)):
            os.makedirs(os.path.dirname(cov_subdir))
         if not os.path.exists(out_dir):
            os.makedirs(os.path.dirname(out_dir))

         np.savetxt(cov_subdir, predict,
            header=" ".join(y_names), comments='')
         print_star2(star, predict, y_names, table_curr, table_init,params)
         plot_star_old(star, predict, y_names, out_dir, reg.sn)

         print("instantiations after range check", star_data.shape)
 
         table_curr.close()
         table_init.close()
         paper_table= os.path.join(baseout, 'tables', star+"_paper_"+reg.sn+".dat") 
         paper_out(star, star_data, predict, paper_table,params,y_names)
         print("\ntotal prediction time:", sum(run_times))
         print("time per star:", np.mean(run_times), "+/-", np.std(run_times))
         print("time per perturbation:", np.mean(np.array(run_times) / 10000), "+/-",
         np.std(np.array(run_times) / 10000))
         print("\ncouldn't process", wrong_cols)
         print("out of bounds", outside)
         return y_names, predict
  
def process_dir(data, rpath, params):
   """Necessary steps to train our algorithm and evaluate our data. 
      I have had to break this routine up into process_dir and regress_star.
      The reason being is to do a check on subgiants. 
      Usually we assume ratios and slopes and intercepts will work but we run into trouble with SGs.
      So we will run with the features as is. Then do some checks and a second run if we think we have a subgiant"""
   
   ### Initialize and Define. 
   roots=[]
   files=[]
   stars=[]   

   def_Xcols=['Teff','Dnu0','Fe_H']
   def_Ycols=['M','age','radius','alpha','ev_stage']
   
   
   ### Get the directories and files. 
   for root, subdir, path in os.walk(rpath):
      if len(path) > 0:
         for p in path:
           if p.endswith(params.perturb_pattern):
              roots.append(root)
              files.append(os.path.basename(p).replace(params.perturb_pattern, '')) 
              stars.append(os.path.join(root,p))

   ### Read in the star data 
   for j, star_fname in enumerate(stars):
       
       star = os.path.split(star_fname)[-1].split("_")[0]
       star_data = pd.read_csv(star_fname, sep='\t')
       sdrop=[x for x in list(star_data) if x in params.ex]

       star_data = star_data.drop(sdrop,  axis=1).dropna()
       Ycols=params.y_init+params.y_curr
       
       
       ### Need to post-process data based on what we do to slope and intercept:
       if params.train_int_slope is True :
           #print(list(data))
           if not any( x in list(data) for x in  ['r02c','r01c','r10c','r13c'] ):
               print('Slopes and intercepts not present in training data. Assumed they have been dropped')
           
               
           if params.train_int_slope_all==1:
               print('Requesting that the slopes and intercepts are calculated from those modes availible in the observational data.')
               print('Recaclulating but not pickling')        
               data=calc_inct2(data, list(star_data))
               

       baseout=roots[j].replace(params.perturb_dir,params.out_dir, 1)
       print(baseout) 
       ynames, predict=regress_star(star, star_data, Ycols, data,params,baseout)
       
       baseoutDF=baseout+'_DF'
       star_data2=star_data.loc[:,def_Xcols]
       ynames, predict=regress_star(star, star_data2, def_Ycols, data,params,baseoutDF)
       
       
       """
       ### Lets first check if ratios and their intercepts/slopes are present. Otherwise we are already done. 
       sdrop2=['r02c','r01c','r10c','r13c','r02','r01','r10','r13','dnu02','r02m','r01m','r10m','r13m']
       xnames=list(star_data)
       if any( x in xnames  for x in  sdrop2 ):       
          ###For now check if Dnu < 70 as to whether we should rerun
          if  'Dnu0' in xnames:
            Dnu0=star_data['Dnu0'].mean()
            if Dnu0 <= 70:
               print('Dnu0 <= 70, Need to drop ratios and slopes to see if we might have a sub giant')  
               ### Should probably reload star_data to be safe but lets see how we go without
               sdrop2=[x for x in sdrop2 if x in xnames]
               #print('xnames',xnames)
               #print('sdrop2',sdrop2)
               star_data = star_data.drop(sdrop2,  axis=1).dropna()
               baseoutSG=baseout+'_SG'
               params.drop_intercepts=True
               print(baseoutSG) 
               ynames, predict=regress_star(star, star_data, data,params,baseoutSG)
               if 'ev_stage' in ynames:
                 idx2=list(ynames).index('ev_stage')
                 ev=predict[:,idx2].mean()
                 if ev > 2.0:
                  print('Probably a subgiant as most solutions pointing to  ev_stage=3')
                 else:
                   print('Star probably still on MS. OK to use ratios and their intercepts/slopes')  
               else:
                print('ev_stage not in ynames. Rerun grid beyond MS if you want diagnostic') """
       
def split_rs(rat):
    parts=rat.split('_')
    return r'{}'.format(parts[0])+'$_{{}}$'.format(parts[1])


def load_data(params):
   """Options for reading in data. Here we specify columns we would like to drop from our analysis.""" 

   nuc_network=['X','Y','Li','Be','B','C','N','O','F','Ne','Mg','H1','H2','He3','He4','Li7',
               'Be7','B8','C12','C13','N13','N14','N15','O14','O15','O16','O17','O18','F17',
               'F18','F19','Ne18','Ne19','Ne20','Ne22','Mg22','Mg24']
   ratios = ['r02','r01','r13','r10']
   
 
 
   ### Pickle training data for speed 
   if params.repickle_grid is True: 
      data=pd.read_csv(params.fname, sep='\t')
      ### Caclulate slopes and intercepts if we havent already 
      if 'r02c' not in list(data):
         print('Need to calculate intercepts. Will take a few minutes') 
         data=calc_inct2(data, list(data))
      data.to_pickle(params.picklen)        
      
   else:
      if os.path.isfile(params.picklen):
         print('Reading Pickle') 
         data=pd.read_pickle(params.picklen)
      else:   
         data=pd.read_csv(params.fname, sep='\t')               
         ### Caclulate slopes and intercepts if we havent already 
         if 'r02c' not in list(data):
            print('Need to calculate intercepts. Will take a few minutes') 
            data=calc_inct2(data, list(data))
         data.to_pickle(params.picklen)
   
   #print('load_data', list(data))    
   
   if 'Fe_H_out' not in list(data):
       data['Fe_H_out']=data['Fe_H']
   
   ### Load the default forest to help with slopes and intercepts
   load_default_forest(params,data)
   
   ### Drop Abundances
   f1 = lambda x: x+'_c'
   f2 = lambda x: x+'_surf'

   f3=lambda x: r'{}'.format(x)+'$_c$'
   f4=lambda x: r'{}'.format(x)+'$_{\\rm{surf}}$'
   
   ### Based on network include and exclude 
   inc_network=list(filter(None, [x.strip() for x in params.inc_network.split('|')]))
   exclude2 = [x for x in nuc_network if x not in inc_network]
   exclude2=[f(x) for x in exclude2 for f in (f1,f2)]
   
   ### Based on surface or central abundances 
   if params.drop_abund_loc==0:
      exclude2 += [f(x) for x in nuc_network for f in (f1,f2)]   
   elif params.drop_abund_loc==1:
       exclude2 += [f1(x) for x in nuc_network]       
       for x in nuc_network:
          y_latex_short[f2(x)]=f4(x) 
          y_latex[f2(x)]=f4(x)   
   elif params.drop_abund_loc==2:
       exclude2 += [f2(x) for x in nuc_network]
       for x in nuc_network:
          y_latex_short[f1(x)]=f4(x) 
          y_latex[f2(x)]=f3(x) 
   
  
   ### Drop Ratios according to instantiations in parameter file
   
   ### This weeds out the ratios at all radial orders
   if params.irat < 2:
      exclude2+= [x for x in list(data) for y in ratios if  re.search(y+'_''.',x) ]
   
   ### This weeds out summary global ratios
   if params.irat ==0:
      exclude2+=ratios 
   
   ### This weeds out all instances (global, radial order, intercept) of a ratio specified in the exclude list
   if params.irat ==3:
       rrdrop=[x for x in ratios if x in params.exclude.split('|')]
       exclude2+= [x for x in list(data) for y in rrdrop if  re.search(y+'.',x) ]
   
   ### Drop intercepts if asked to do so
   if params.drop_intercepts is True:
       exclude2 += [x+'c' for x in ratios]        
       exclude2 += [x+'m' for x in ratios]        

   ### remove columns from the exclude list   
   dcols=[i for i in data.columns if i in params.exclude.split('|')]  
  
   ### Actually drop them 
   data = data.drop(dcols, axis=1)

   ### Only need to drop the columns if they are in the training grif. 
   exclude2=[x for x in exclude2 if x in data.columns]
   data=data.drop(exclude2,axis=1)
   

   
   ### Pick training data by evolutionary phase XXX Fix so that this is a list
   if 'ev_stage' in list(data):
      if params.evphase > 0:
         data=data.loc[data['ev_stage'] == params.evphase]
   else:
      print('ev_stage not in xcols. Not trying to drop phase')   


   ### Not dealing with 'Fe/H' label.    
   try:
     data.rename(columns={'Fe/H':'Fe_H'}, inplace=True)
   except : pass

   ### Store all the excluded columns 
   params.ex=exclude2+[a.strip() for a in params.exclude.split('|')]

   ### By now only ratios not in the y dictionary of latex labels. Lets add these. 
   ### intercept and slope are fine in plain text for now. Will be replotted for papers anyway 
   ks=y_latex.keys() 
   for x in list(data):
      if x not in ks:
         if '_' in x:
            val=split_rs(x)
         else: val=x
         y_latex_short[x]=val
         y_latex[x]=val 

   
   ### Store parameter limits    
   params.maxs = data.max()
   params.mins = data.min()

   
   return data 




def train_test(data, params, MS=True):
  """Here we drop some tracks from our grid and see how well our RF predicts the 
     missing data. We use an explained_variance_score to get an initial indication. 
     MS=true is for the MS grid and drops models appropriately from there. 
     It was unclear to me at the time of writing whether we need special treatment 
     for different phases, hence the MS flag.""" 
  
  if MS is True:
      
    ### Something Earl needs probably for inversions.  
    if '03' in list(data):  
      data=data.drop(['O3','O1','O2','FF'],axis=1) 
      
    ### Get the Sun observational data  
    sundat=pd.read_table('RF/Validate/SunTest_1/sun-freqs/sun-freqs_perturb.dat')
    sundat=sundat.drop(['nu_max'],axis=1)
    
    
    ### Slice sizes for breaking up training data 
    for skip in [16,8,4,2,1]:
      div=32/skip
      print('Dropping every',skip, 'models per track')  
                  
      ###Validation on MS Grid
      Tdata=data.iloc[::][::skip]
      #Tdata.reset_index() 
      train_idx=[]
      dev_idx=[]


      ### We have dropped models from the tracks, but we need to pull out entire tracks as well.       
      for jj in range(len(Tdata)):
         n=jj//div
         if n%2==0:
          train_idx.append(jj)
         else: dev_idx.append(jj)
         
      ### These are want our inputs and predictions are    
      X_columns = ['Teff', 'Fe_H', 'Dnu0', 'dnu02', 'r02', 'r01', 'dnu13', 'r13', 'r10']
      Y_columns= ['M', 'Y', 'Z', 'alpha', 'overshoot', 'diffusion', 'age', 'X_c', 'mass_X', 'mass_Y', 'X_surf', 'Y_surf', 'radius', 'L']
      params.y_init=Y_columns
      params.y_curr=[]
      data_train=Tdata.iloc[train_idx][X_columns+Y_columns]
      #print(data_train)
      X_dev, Y_dev=Tdata.iloc[dev_idx][X_columns],Tdata.iloc[dev_idx][Y_columns]

      ### Now need to drop rows where there is NAN. 
      idx=X_dev.isnull().any(axis=1)  
      kdx=[j for j in X_dev.index if idx.loc[j]  == True]
      idx=Y_dev.isnull().any(axis=1)    
      kdx+= [j for j in Y_dev.index if idx.loc[j]  == True]  
      kdx=list(set(kdx))
      X_dev=X_dev.drop(kdx)
      Y_dev=Y_dev.drop(kdx)
     

      ### regs is the regressor can compare SKlearn and TensorFlow is so inclined
      for reg in params.regs:
       #reg.train_regressor(data_train, X_columns,y_show=Y_columns)
       reg.train_regressor(data_train, X_columns,params)
       star_X = X_dev.loc[:,X_columns]
       Xpred=reg.forest.predict(star_X)
       star_X = sundat.loc[:,X_columns]
       Spred=reg.forest.predict(star_X)
       #print(Xpred)
       if reg.sn=='tf':
           Xpred=Xpred['scores']


       ### Explained Variance score for the listed parameters             
       for y in ['M','Z','age','radius','L','diffusion']:
          idx= Y_columns.index(y)
          score=explained_variance_score(Y_dev[y],Xpred[:,idx])
          print(reg.sn,':', y, 'Explained Variance',score)
          
       ### Predictions for the sun    
       for y in ['M','Z','age','radius','L','diffusion']:
          idx= Y_columns.index(y)
          smean=Spred[:,idx].mean()
          sstd=Spred[:,idx].std()
          print(reg.sn,':', y, 'Sun pred: ',r'{:0.3f} pm {:0.3f}'.format(smean,sstd))



    ### Special case where we use all the training data 
    data_train=data[X_columns+Y_columns]
    for reg in params.regs:
       reg.train_regressor(data_train, X_columns,params)
       star_X = sundat.loc[:,X_columns]
       Spred=reg.forest.predict(star_X)
       print(Spred.shape)
       print('full data')
       for y in ['M','Z','age','radius','L','diffusion']:
          idx= Y_columns.index(y)
          smean=Spred[:,idx].mean()
          sstd=Spred[:,idx].std()
          print(reg.sn,':', y, 'Sun pred: ',r'{:0.3f} pm {:0.3f}'.format(smean,sstd))
       

     

if __name__ == "__main__":
   #################################################################################
   ### Start; Assumes all files to be processed are inside a 'star' directory    ###
   ### This directory is in data and/or perturb.                                 ###
   ### -p flag will create perturbed files from the observational data           ###
   ### -v will split data into test and dev for validation not input obs req     ###
   #################################################################################

   ### Parse args
   parser = argparse.ArgumentParser(description='Adding tensorflow capability to Bellinger et al. (2016) SKlearn RF')
   parser.add_argument(dest="parentd", type=str,
                    help="Directory Name in $data_dir or $perturb_dir in which to process")

   parser.add_argument('-o', dest="parmfile", type=str, default="./RF/RF_in.parms",
                    help="Location of RF parmaterfile")


   parser.add_argument("-p", "--perturb",action="store_true", default=False,
                    help="(re)perturb the data")

   parser.add_argument("-v", "--validate",action="store_true", default=False,
                    help="Split into training and dev set and report")

   args = parser.parse_args()



   params=pr.parameters(args.parmfile)

   ### for reproducibility
   np.random.seed(seed=params.random_seed) 

   ### Perturb data if required
   if args.perturb is True:
     lp.run_perturb(args.parentd, params)
   
   ### RF Regressor(s) to use
   if params.lib==0 or params.lib > 2:
     skrf=sk_rf()
     tfrf=tf_rf()
     regs=[skrf, tfrf]
   elif params.lib==1:
      skrf=sk_rf()
      regs=[skrf]
   elif params.lib ==2:
      tfrf=tf_rf()
      regs=[tfrf]

   params.regs=regs


   ### Load grid of models 
   data = load_data(params)
   rpath=os.path.join(params.perturb_dir, args.parentd)

   ### Either Run the code or perform a validation test
   if args.validate is False:
       process_dir(data, rpath, params)
   else:
       train_test(data,params)

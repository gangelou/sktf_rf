#! /opt/anaconda-3/anaconda3/bin/python3


import os
from sys import argv
import re
from multiprocessing import Pool 
import pandas as pd
import sys
import multiprocessing as mp
import numpy
import shutil
import numpy as np
import RF.lim_pertfreqs as lpf
import argparse
import pickle
import RF.param_read as pr
import pprint
from time import time


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]



class workers(object):
   """Workers for multiprocessing map"""
   
   def __init__(self, obs_data, xcols, lims): 
      self.obs_data=obs_data
      self.xcols=xcols
      self.lims=lims  
      self.limslope=0
      
   def rand_inst(self,inst):
       """Routine for pertubing obs_data"""
       ### Set up an empty array full of negative ones
       inst=np.array([-1]*len(self.obs_data['value']))
       
       ### I think I need this to stop drawing the same result from every thread. Too scared to remove and test again 
       np.random.seed()
       
       ### This is obtuse. We enter with some of our parameters outside the grid limits.
       ### While any of our parameters are outside their limits we keep rerolling.
       ### We can exit when we have an instantiation with all our obs parameters inside the grid limits        
       while    all( [self.lims[col.strip()][0] < inst[i]   < self.lims[col.strip()][1] for i,col in enumerate(self.xcols)] ) is False  : 
              inst= np.random.normal(self.obs_data['value'],self.obs_data['uncertainty'])
       return inst



   def rand_instf(self,nmx): 
     """ Routine for perturbing frequencies"""
     
     ### Set up an array of our paramrers full of negative ones
     ### Also set up an an array for our modes. 
     inst=np.array([-1]*len(self.fcols))
     lfd=len(self.freq_data['l'])
     fd2=np.full((lfd),-1, dtype=[('l','int'),('n','int'),('v','float')])
     fd2['l']=self.freq_data['l']
     fd2['n']=self.freq_data['n']
     np.random.seed()

     ### This is obtuse. We enter with some of our parameters outside the grid limits.
     ### We pertub the frequencies and calculate our seismic parameters 
     ### While any of our parameters are outside their limits we keep rerolling.
     ### We don't have gradients and intercepts in our training data. They are post processed so we need to ignore them and hope for the best. 
     ### We enforce that freqencies come back with the original mode ordering. 
     ### We can exit when we have an instantiation with all our obs parameters inside the grid limits                   
     while    all( [self.lims[col.strip()][0] < inst[i]   < self.lims[col.strip()][1] for i,col in enumerate(self.fcols) if col not in ['r01c','r01m','r10c','r10m','r13m','r13c','r02c','r02m']] ) is False :
       fd2['v']=[np.random.normal(self.freq_data['v'][i],self.freq_data['err'][i]) for i in range(lfd)]
       fd2_sort=np.sort(fd2, order='v')
       
      
       
       ### Force the same mode ordering
       if not all(fd2_sort['l']==self.lorder) or not all(fd2_sort['n']==self.norder):
           print(all(fd2_sort['l']==self.lorder))
           print(all(fd2_sort['n']==self.norder))
           continue  
      
       inst=lpf.get_freqs(self.fcols, np.asarray(fd2), nmx,self.limslope)
       
       ### Force we come back within the ranges in our grid
       for i,col in enumerate(self.fcols):
         if col not in  ['r01c','r01m','r10c','r10m','r13m','r13c','r02c','r02m']:  
          if not self.lims[col.strip()][0] < inst[i]   < self.lims[col.strip()][1]  :
             print(self.lims[col.strip()][0], self.lims[col.strip()][1], inst[i],col )  
     
     #self.count +=1
     #sys.stdout.write(str(self.count)+ "\r")
     return inst  


   def rand_instf_obo(self,nmx):
     """Special routine to perturb frequencies one-by-one. 
        Might be necessary for stars with highly uncertain frequencies"""  
       
     ### As above set holding arrays, But make sure modes are in order by frequency  
     inst=np.array([-1]*len(self.fcols))
     lfd=len(self.freq_data['l'])
     fd2=np.full((lfd),-1, dtype=[('l','int'),('n','int'),('v','float')])
     fd2['l']=self.freq_data['l'][self.vorder]
     fd2['n']=self.freq_data['n'][self.vorder]
     np.random.seed()
     
     
     ### Outide the parameter limits so enter here 
     while    all( [self.lims[col.strip()][0] < inst[i]   < self.lims[col.strip()][1] for i,col in enumerate(self.fcols) if col not in ['r01c','r01m','r10c','r10m','r13m','r13c','r02c','r02m']]) is False : 
        ### Need to roll one by one iretry times. Lets generate and store freq 1
        idx=self.vorder[0]
        first=[np.random.normal(self.freq_data['v'][idx],self.freq_data['err'][idx]) for j in range(self.iretry)]
        failed={}

        ###We will retry only so many times
        for i in range(self.iretry):
            prev=first[i]
            fd3=[prev] 

            ###lets go through the modes and generate a few to try here  
            for j, idx in enumerate(self.vorder[1:]):
       
               ### This is a bit silly but not all that expensive to do.
               ### generate irety perturbations of the next mode
               ### from the list of perturbed frequencies find any that are larger than the previous mode                
               nus=[np.random.normal(self.freq_data['v'][idx],self.freq_data['err'][idx]) for k in range(self.iretry)]
               jdx=[k for k in range(self.iretry) if nus[k] > prev]  
               
               ### If we have a frequncy greater than previous nu add it to the list and move to the next mode. We only try the first freq in jdx. 
               ### If we can find a larger frequency break this loop and start the search again with a new initial freq  
               if len(jdx) ==0:
                  failed[i]=idx
                  break  ### stop going through the vorders and restart
               else:
                   jdx=jdx[0]
                   prev=nus[jdx]
                   fd3.append(prev)

            ### Do we have all the perturbed freqs   
            if len(fd3)==len(self.vorder):
               fd2['v']=fd3
               break      ###the retrying  
            else:
              #Cycled through 
              #print(failed)
              #print('returning a fail')
              return [] 
        #print(fd2)  
        inst=lpf.get_freqs(self.fcols, np.asarray(fd2), nmx,self.limslope)

     return inst     
                          
             

def pickle_lims(limfile,fname):

   ### Not going to search for the pickled training data incase its a new file. 
   data=pd.read_csv(fname, sep='\t')
   lims={}

   for col in list(data):
     lims[col]=(min(data[col]),max(data[col]))
   
   if 'Fe/H' in lims.keys():
     lims['Fe_H']=lims['Fe/H']

   if 'nu_max' not in lims.keys():
      lims['nu_max']= (139,8110)

   with open(limfile, 'wb') as handle:
    pickle.dump(lims, handle, protocol=pickle.HIGHEST_PROTOCOL) 
   return lims



def run_perturb(pdir, parms):
   """Ptyhon code to pertub observational data for the Random Forest"""   

   ### Pickle the limits from the training data so that all parameters remain within our grid. 
   ### We pickle as we only want the limits and not have to read the whole grid in each time. 
   parms.limfile=os.path.join('./RF/Pickles/', parms.simfile[:-4]+'_lims.pickle')
   
   if parms.repickle_lims is True:
      lims=pickle_lims(parms.limfile, parms.fname)
   else: 
      if os.path.isfile(parms.limfile):
         try:
            with open(parms.limfile, 'rb') as handle:
               lims = pickle.load(handle)
         except Exception as e:
            print(e) 
            print('Error with pickled limfile. Trying again')
            lims=pickle_lims(parms.limfile,parms.fname)
      else:
         print('No pickle file for RF limits: Pickling')  
         lims=pickle_lims(parms.limfile,parms.fname)
	
	
	
   ### Need a perturb dir
   if not os.path.isdir(parms.perturb_dir):
       os.mkdir(parms.perturb_dir)


   ### First make sure we have the feeder directory
   rpath=os.path.join(parms.obs_dir,pdir)
   if not os.path.isdir(rpath):
      print('Error: Cannot find obs_dir in the data path')
      print(rpath)
      sys.exit(2)  
 

   roots=[]
   files=[]
   ### Get the directories and files. Based on the fact we always provide a -obs file
   for root, subdir, path in os.walk(rpath):
     if len(path) > 0:
        for p in path:
           if p.endswith(parms.obs_pattern):
             roots.append(root)
             files.append(os.path.basename(p).replace(parms.obs_pattern, '')) 




   res = mp.cpu_count() 
   for jj in range(len(files)):
      f=os.path.join(roots[jj],files[jj]) 
      xcols=[] 
      obs_data=None
      freq_data=None
 
      obs_file=f+parms.obs_pattern
      freq_file=f+parms.freq_pattern



      ### first obsdata       
      if os.path.isfile(obs_file):  
         obs_data= pd.read_table(obs_file,delim_whitespace=True)
         obs_data.columns=[x.lower() for x in obs_data.columns]    
         xcols+=list(obs_data['name']) 
         xcols = [x if x !='Fe/H' else 'Fe_H' for x in xcols] 
         
         ### We drop any parameter we dont have limits for. Sorry  
         dropl=[]
         lk=lims.keys()
         for i, col in enumerate(xcols):
            if col not in lk:
               dropl.append(i)
       
         ###Also the label Fe/H causes headaches. I refuse to deal with this   
         xcols = [x for i,x in enumerate(xcols) if i not in dropl]  
         obs_data.drop(dropl, inplace=True)

         print('Perturbing from obsfile: ')
         print(obs_file)
         pprint.pprint(list(chunks(xcols, 10)))

         
         
         ### Inialise worker class for multiprocessing and perturb obsdata
         work=workers(obs_data, xcols, lims)
         for i in range(len(xcols)):    
            with mp.Pool(res) as p:
               results= p.map(work.rand_inst, range(parms.npurt))


         ### I want the star name for writing the outputfile later
         sname=f.split('/')[-1]
         
         ### Create the output dir for the pertub files 
         dirout=roots[jj].replace(parms.obs_dir, parms.perturb_dir,1)
         print(dirout)
         os.makedirs(dirout,exist_ok=True)

     
      ### Get numax loc for frequency perturbations 
      ix=xcols.index('nu_max') 
      nmxs=[res[ix] for res in results]
      rescomb=results
      fcols=[]
      
      ### Try whitespace then csv delimited freq files 
      if os.path.isfile(freq_file):
         try:
           freq_data=np.genfromtxt(freq_file, delimiter='', dtype=[int,int,float, float], names=True) 
         except: freq_data=np.genfromtxt(freq_file, delimiter=',', dtype=[int,int,float, float], names=True)
         
         ### Rename incase people use different nomenclature 
         dnames={'nu':'v', 'dnu':'err'}
         nnames= freq_data.dtype.names
         names=[ dnames[x] if x in dnames else x for x in nnames ]
         freq_data.dtype.names=names
      
         #freq_data=pd.read_table(freq_file,delim_whitespace=True)
         
         ### Lets work out which spherical degrees we are dealing with 
         lk2=['Dnu0', 'dnu02', 'dnu13', 'r02','r01','r13','r10']
         summrat=['r02','r01','r13','r10']

         fcols=[x for x in lk2 if '0' in x or '1' in x]    
         if 3 not in freq_data['l']:
           fcols=[x for x in fcols if '3' not in x]
         if 2 not in freq_data['l']:
           fcols=[x for x in fcols if '2' not in x]         
         if 1 not in freq_data['l']:
           fcols=[x for x in fcols if '1' not in x]         


         ### Just because we have the degrees present. It doesn't mean we have the necessary radial orders
         ### To do anything useful.
         rdict, get_rd=lpf.get_freqs(fcols, freq_data, nmxs[0], parms.train_int_slope_all, get_rs=True) 
         
         for col in fcols:
            if col in summrat:
               if col not in rdict.keys():
                   print(col, 'not in rdict which means could not calculate it from freqs. Dropping')
                   fcols.remove(col)          
 

         print(fcols)
         ### This is a pass through once to work out which columns (radial orders) we need to calculate.
         ### Pertains to the case when we want individual ratios
         if parms.r_n == True:

           for key, olist in get_rd.items():
               for nord in olist:
                  par= key+'_'+str(nord)
                  if par in lims.keys():
                    if not any(np.isnan(lims[par])):
                       ### Filter only those in radial_incl  
                       if len(parms.radial_incl) > 0:
                        if nord in parms.radial_incl:
                           fcols.append(par)
                       ### If empty list include evertyhing    
                       else: 
                           fcols.append(par)
                           


                  
         ### Might be reading in training data with different columns that we expect 
         fcols=[f for f in fcols if f in lk]   

 
 

         ### The following is commented out. Generally we want to perturb what we can
         ### Then filter out at the training stage. Break in case of emergency.  
         """
         ### Might want to drop summary
         
         if parms.irat==4:            
            fcols=[x for x in fcols if x not in summrat]

         ### Drop the radial orders we want to exclude              
         for ro in parms.radial_excl:                
            fcols=[x for x in fcols if not x.endswith('_'+str(ro))] """
  
  
         ### Need to do some special work to calculate intercepts and slopes 
         if parms.train_int_slope:
            for rat in summrat:
                #if any([rat in fcol for fcol in fcols if rat not in parms.exclude]):
                if any([rat in fcol for fcol in fcols]):
                    fcols.append(rat+'m')
                    fcols.append(rat+'c')
 
 
         ### We want to enforce that the modes keep the same order after having their frequencies perturbed. 
         #Lets store their current ordering           
         print('Perturbing from freqs file: ')
         pprint.pprint(list(chunks(fcols, 10)),width=160)   
         fsort=np.sort(freq_data,order='v')
         work.lorder=fsort['l']
         work.norder=fsort['n']
         work.limslope=parms.train_int_slope_all
         work.fcols=fcols
         work.freq_data=freq_data  


         ### If the frequencies are highly uncertain it might be necessary to perturb them one by one
         ### And to take special care to keep the right ordering. Call a specific pertub routine for such cases. 
         if parms.obo is False:
            with mp.Pool(res) as p:
               results2= p.map(work.rand_instf,nmxs)

         else:
            work.iretry=parms.obo_retry
            work.vorder=np.argsort(freq_data, order='v')

            with mp.Pool(res) as p:
               results2= p.map(work.rand_instf_obo,nmxs)  


               
         ### Results2 might come back empty if we fail the one by one for uncertain stars      
         ll=[len(x) for x in results2]
         jdx=[x for x in range(len(ll)) if ll[x]==0]
         ljdx=len(jdx)
         if  ljdx > 0:
            print('Warning dropping', ljdx, 'instantiantions. Will evaluate', parms.npurt-ljdx, 'instances')
            results=[results[j] for j in range(len(results)) if j not in jdx]
            results2=[results2[j] for j in range(len(results2)) if j not in jdx]         
         
         ### Combine obs and freq perturbed arrays
         rescomb=np.hstack((numpy.asarray(results),numpy.asarray(results2))) 
      else:
        print('Warning No Freq file found')         
        
      ### Save and exit   
      mcols="\t".join(xcols+fcols)
      numpy.savetxt(os.path.join(dirout,sname+parms.perturb_pattern),rescomb,delimiter=' \t',header=mcols, comments='' )




if __name__ == "__main__":
   """Initialization: Running the perturbation code in isolation. """
  
   start=time()
   parser = argparse.ArgumentParser(description="GCA's Python3 routine to perturb observational data")
   parser.add_argument(dest="parentd", type=str,
                    help="Directory Name in $data_dir or $perturb_dir in which to process")


   parser.add_argument('-o', dest="parmfile", type=str, default="./RF/RF_in.parms",
                    help="Location of RF parmaterfile")


   parser.add_argument('-t', action='store_true',
                    dest='r_n',
                    help='Calculate ratios at every order')

   parser.add_argument('-r', action='store_true',
                    dest='repickle',
                    help='Re-pickle the RF limits')

   args = parser.parse_args()
   parms=pr.parameters(args.parmfile)

   ### Overwrite parameter file options

   if args.r_n is True:
       parms.r_n=args.r_n
       
   if args.repickle_lims is True:
       parms.repickle_lims=args.repickle

   ### Do the Perturbing   
   run_perturb(args.parentd, parms) 
   end=time()
   print()
   print("%.5g seconds to train regressor" % (end-start))
   print()







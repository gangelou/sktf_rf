#! /scratch/seismo/angelou/Python3.5/anaconda3/bin/python3


import os
from sys import argv
import re
from multiprocessing import Pool 
import pandas as pd
import sys
import multiprocessing as mp
import numpy
import shutil
import weighted
import math
from scipy import stats
from scipy.stats import norm
import numpy as np


def check_mon(x,y):
   ds=np.sign(np.diff(y))
   if len(ds) > 2:
      if ds[0] != ds[2] and ds[1] !=ds[2]:
          x=x[2:]
          y=y[2:] 
          return x,y  
      if ds[0] != ds[1] :
          x=x[1:]
          y=y[1:]  

      signs=[idx for idx, i in enumerate((np.diff(np.sign(y)) != 0)*1) if i==1]
      if 0 in signs and 1 in signs and 2 in signs:
        x=x[3:]
        y=y[3:]
        return x,y
      if 0 in signs and 1 in signs:
        x=x[2:]
        y=y[2:]
        return x,y
      if 0 in signs:   
        x=x[1:]
        y=y[1:]

   return x,y

def lin_reg (x,y,wts):
    try:
      #slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
      slope, intercept = np.polyfit(x,y,1,w=wts)
      return slope, intercept
    except:
        return np.nan, np.nan


def getslopes(ns, rats, fcols, lim, wts, srat):
    """I dont want these sorted. I think it will muck up the weights"""
    x=ns
    y=rats
    if lim ==1 :
       idx=[ro for ro in ns if srat+'_'+str(ro) in fcols]
       x=[ns[j] for j in idx]
       y=[rats[j] for j in idx] 
       
       
       ### Sort and check for montonicity
       #idx=np.argsort(np.asarray(x))
       #x=[x[i] for i in idx]
       #y=[y[i] for i in idx]
       #x,y=check_mon(x,y)
    slope, intercept = lin_reg(x,y,wts)
    #print (x,y,slope, intercept)
    return slope, intercept


def calc_dd01(l0, l1):
    #dd_01= 1/8( nu_[n-1,0] - 4*nu_[n-1,1] + 6*nu_[n,0] - 4*nu[n,  1] + nu_[n+1,0] )
    dds = []
    weights = []
    dnu = []
    dd01n=[]
    for i, mode in enumerate(l0):
        n = mode['n']
        np1 = n + 1
        nm1 = n - 1

        if nm1 in l0['n'] and np1 in l0[
                'n'] and nm1 in l1['n'] and n in l1['n']:
            adx = numpy.where(l0['n'] == nm1)[0][0]
            bdx = numpy.where(l1['n'] == nm1)[0][0]
            cdx = numpy.where(l0['n'] == n)[0][0]
            ddx = numpy.where(l1['n'] == n)[0][0]
            edx = numpy.where(l0['n'] == np1)[0][0]
            dd01 = 0.125 * (l0['v'][adx] - 4 * l1['v'][bdx] +
                            6 * l0['v'][cdx] - 4 * l1['v'][ddx] + l0['v'][edx])
            dds.append(dd01)
            weights.append(i)
            dnu.append(l1['v'][ddx] - l1['v'][bdx])
            dd01n.append(n)
    return (dds, dnu, weights,dd01n)



def calc_dd10(l1, l0):
    # dd_10=-1/8( nu_[n-1,1] - 4*nu_[n,  0] + 6*nu_[n,1] - 4*nu[n+1,0] +
    # nu_[n+1,1]
    dds = []
    weights = []
    dnu = []
    dd10n=[]
    for i, mode in enumerate(l1):
        n = mode['n'] 
        np1 = n + 1
        nm1 = n - 1


        if nm1 in l1['n'] and n in l0['n'] and n in l1[
                'n'] and np1 in l0['n'] and np1 in l1['n']:
            adx = numpy.where(l1['n'] == nm1)[0][0]
            bdx = numpy.where(l0['n'] == n)[0][0]
            cdx = numpy.where(l1['n'] == n)[0][0]
            ddx = numpy.where(l0['n'] == np1)[0][0]
            edx = numpy.where(l1['n'] == np1)[0][0]
            dd10 = -0.125 * (l1['v'][adx] - 4 * l0['v'][bdx] +
                                 6 * l1['v'][cdx] - 4 * l0['v'][ddx] + l1['v'][edx])
            dds.append(dd10)
            weights.append(i)
            dnu.append(l0['v'][ddx] - l0['v'][bdx])
            dd10n.append(n)   
    return (dds, dnu, weights,dd10n)





def consecutive(seq):
    """May not have consecutive freqs to calc dnu"""
    weights = []
    dnus = []
    for i in range(len(seq) - 1):
        if seq['n'][i + 1] - seq['n'][i] == 1:
            weights.append(i+1)
            dnus.append(seq['v'][i + 1] - seq['v'][i])

    return (weights, dnus)


def pair_pmodes(lm, ln):
    """ We assume that the modes are all p modes here 
        and labelled correctly. We are perturbing observations
        so don't need all the checks and balances when dealing 
        with pulsation code output """

    #d02- vnl - v n+1, l-2
    lmsind = []
    lnsind = []
    
    #print(lm,ln)
    for i, row in enumerate(lm):
        n=row['n']  
        idx=np.where(ln['n']==n-1)[0]
        if len(idx) ==1:  
          idx=idx[0]
          #print(idx, lm['n'][idx],lm['v'][idx], i, ln['n'][i],ln['v'][i])
          lmsind.append(i)
          lnsind.append(idx)  

    lms = lm[lmsind]
    lns = ln[lnsind]
    
    return (lms, lns, lmsind)


def calc_rseps(la, lb, lc, el):
    """I'm pretty sure I do this right.
    But some comments will make it easier to check
    la= lesser spherical degree used to calc small separation.
    lb= spherical degree used to calc large separation.
    lc= higher spherical degree used to calc small separation
    el= a flag to chose which separation to calculate"""
    
    
    dnuS = []
    dnuL = []
    rwts = []
    rns=[]
    for i, mode in enumerate(la):
         n = mode['n']
         nm1 = n - 1
         np1 = n +1


         if nm1 in lc['n']:
            cdx=  numpy.where(lc['n'] ==nm1)[0][0]

            ### Dnu here is V(n) - V(n-1). Applicable for r02 and r01
            if el ==0 and n in lb['n'] and nm1 in lb['n']: 
                  adx = numpy.where(lb['n'] == n)[0][0]
                  bdx = numpy.where(lb['n'] == nm1)[0][0]
                  dnuS.append(la['v'][i] - lc['v'][cdx])
                  dnuL.append(lb['v'][adx] - lb['v'][bdx])
                  rwts.append(i)
                  rns.append(n)

            ### Dnu here is V(n+1) - V(n) as per r13 and r10
            if el==1 and n in lb['n'] and np1 in lb['n']:   
                  adx = numpy.where(lb['n'] == np1)[0][0]
                  bdx = numpy.where(lb['n'] == n)[0][0]
                  dnuS.append(la['v'][i] - lc['v'][cdx])
                  dnuL.append(lb['v'][adx] - lb['v'][bdx])
                  rwts.append(i)
                  rns.append(n)

    return(dnuS, dnuL, rwts,rns)



def get_freqs(fcols, fd2, nmx, limslope, get_rs=False):
   """rdict -- is a dictionary of results we send back 
      get_rd -- for each of our ratios we store at which n we have results for
      
      If we are calculating ratios at every order we can then correctly 
      label to which n our ratio corresponds in rdict.
      
      get_rs is a flag to indicate where this is a single pass through to 
      first get all the ratios we  CAN calculate. 
      The dictionary of ratios gets added to fcols before we mp.map here.
      
      We already determine whether we are calculating slopes 
      and intercepts of the ratios in lim_perturb. So they will be in fcols. 
      limslope is a flag to tell us whether we use all availible modes to calculate
      the intercept and slope, or only those we haven't excluded. 
      """
      
   rdict={} 
   get_rd={}

   l0 = fd2[numpy.where(fd2['l'] == 0)[0]]
   l1 = fd2[numpy.where(fd2['l'] == 1)[0]]
   l2 = fd2[numpy.where(fd2['l'] == 2)[0]]
   l3 = fd2[numpy.where(fd2['l'] == 3)[0]]

   fwhm = (0.66 * nmx**0.88) / (2 * math.sqrt(2 * math.log(2)))
   gaussian_env = norm.pdf(l0['v'], loc=nmx, scale=fwhm)
   gaussian_env1 = norm.pdf(l1['v'], loc=nmx, scale=fwhm)

   
   wght_idx, dnus =consecutive(l0)
   dnuwts = [gaussian_env[i] for i in wght_idx]
   dnu = weighted.median(np.asarray(dnus),dnuwts)
   if  'Dnu0' in fcols: 
      rdict['Dnu0']=dnu


   if 'dnu02' in fcols: 
      l0s,l2s,iwts= pair_pmodes(l0,l2)
      d02s=[x-y for x,y in zip(l0s['v'],l2s['v'])] 
      d02swts = [gaussian_env[i] for i in iwts]
      #print('d02s',d02s)
      if len(d02s) > 0:
        d02=weighted.median(np.asarray(d02s),d02swts)
        rdict['dnu02']=d02

   if 'dnu13' in fcols:
      l1s,l3s,iwts= pair_pmodes(l1,l3)
      d13s=[x-y for x,y in zip(l1s['v'],l3s['v'])] 
      d13swts = [gaussian_env1[i] for i in iwts]
      if len(d13s) > 0:
         d13=weighted.median(np.asarray(d13s),d13swts)
         rdict['dnu13']=d13       
      
 
   if any(x.startswith('r02') for x in fcols):
      dnuS,dnuL,iwts,rns = calc_rseps(l0, l1, l2,0)
      R02 = [x / y for x, y in zip(dnuS,dnuL)]
      r02wts=[gaussian_env[k] for k in iwts]
      if len(R02) > 0:
         r02=weighted.median(np.asarray(R02),r02wts) 
         #print(r02,R02)
         rdict['r02']=r02  
         get_rd['r02']=rns
         for i,nord in enumerate(rns):          
            rdict['r02'+'_'+str(nord)]=R02[i]
         if 'r02c' in fcols:
             rdict['r02m'],rdict['r02c'] = getslopes(rns,R02,fcols,limslope,r02wts,'r02')
      
      
   if any(x.startswith('r13') for x in fcols):
      dnuS,dnuL,iwts,rns = calc_rseps(l1, l0, l3,1) 
      R13 = [x / y for x, y in zip(dnuS,dnuL)]
      r13wts=[gaussian_env1[k] for k in iwts]
      if len(R13) > 0:
         r13=weighted.median(np.asarray(R13),r13wts) 
         #print(r13,R13)
         rdict['r13']=r13
         get_rd['r13']=rns
         for i,nord in enumerate(rns):
          rdict['r13'+'_'+str(nord)]=R13[i]
         if 'r13c' in fcols:
           rdict['r13m'],rdict['r13c'] = getslopes(rns,R13,fcols,limslope,r13wts,'r13')    

   if any(x.startswith('r10') for x in fcols):
       dds, dnu10, iwts,dd10n=calc_dd10(l1, l0)
       RR10 = [x / y for x, y in zip(dds, dnu10)]
       r10wts=[gaussian_env1[k] for k in iwts]
       if len(RR10) > 0:  
          r10= weighted.median(np.asarray(RR10),r10wts) 
          rdict['r10']=r10
          get_rd['r10']=dd10n
          for i,nord in enumerate(dd10n):
            rdict['r10'+'_'+str(nord)]=RR10[i]
          if 'r10c' in fcols:
             rdict['r10m'],rdict['r10c'] = getslopes(dd10n,RR10,fcols,limslope,r10wts,'r10')   
       
   if any(x.startswith('r01') for x in fcols):
       dds, dnu10, iwts,dd01n=calc_dd01(l0, l1)
       RR01 = [x / y for x, y in zip(dds, dnu10)]
       r01wts=[gaussian_env[k] for k in iwts]
       if len(RR01) > 0:
          r01= weighted.median(np.asarray(RR01),r01wts) 
          rdict['r01']=r01
          get_rd['r01']=dd01n
          for i,nord in enumerate(dd01n):
             rdict['r01'+'_'+str(nord)]=RR01[i]
          if 'r01c' in fcols:
             rdict['r01m'],rdict['r01c'] = getslopes(dd01n,RR01,fcols,limslope,r01wts,'r01')   


   ### We either return the instantiation or a dictionary. The dictionary(ies) tell us what 
   ### quantities we have values for    
   if get_rs is False:
     results=[]
     keys=rdict.keys() 
     for k in fcols:
         if k in keys:
           results.append(rdict[k])
         else:
           ### Hopefull shouldn't happen. Ensures we are outside limits and get a reroll. 
           ### Perturbation to freqs may result in a seismic quantity we want but can't calculate  
           results.append(-999999)  
     return results 
   else:
       return rdict, get_rd

import pandas as pd
import numpy as np
import os
import re
import sys
from sklearn.ensemble import RandomForestClassifier
from scipy.stats import norm
from math import log10
import math
import multiprocessing as mp
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import random
import multiprocessing as mp
from sklearn.metrics import explained_variance_score

class training_data(object):
    """I have decided to turn this into a class. It seemed all the steps I kept adding to filter the 
       training data was making things complicated. Might be uncessary and more code but I hope makes it clear. 
       We first initialise loading the data and adding/renaming FeH columns. Then we can call a method 
       that goes through at applies all the filtering step by step, method by method.""" 
       
    def __init__(self, params):

        self.set_defs()        
        data=self.load_data(params)
        data=self.modify_FeH_labels(data)     
        data=self.repalce_OS(data,params)
        ##Hack
        data['RCZ2']=data['cz_radius']*data['radius']
        data['MCZ2']=data['cz_mass']*data['M_current']
        data['RCC2']=data['radius_cc']*data['radius']
        data['MCC2']=data['mass_cc']*data['M_current']
          
        data['ev_stage']=data['ev_stage2']  
        self.data=data


    def load_data(self,params):
      """Load the training data and calculate slopes and intercepts if necessary.
         Check if this is not our first rodeo and load from pickle"""  
        
      ### Pickle training data for speed 
      if params.repickle_grid is True: 
         data=pd.read_csv(params.fname, sep='\t')
         data=self.mp_calc_newcolumns(data, list(data))
         data.to_pickle(params.picklen)       
      else:
         if os.path.isfile(params.picklen):
           print('Reading Pickle') 
           data=pd.read_pickle(params.picklen)
         else:
            data=pd.read_csv(params.fname, sep='\t')
            data=self.mp_calc_newcolumns(data, list(data))
            data.to_pickle(params.picklen)  

      return data

        
    def modify_FeH_labels(self,data):
       """Give Fe/H a proper label pandas and numpy can deal with. 
          Copy the Fe_H column so we can use it to infer the output metallicity. 
          I know it seems weird but its a sanity check"""
          
       ### Not dealing with 'Fe/H' label.    
       try:
          data.rename(columns={'Fe/H':'Fe_H'}, inplace=True)
       except : pass 
        
       ### Fe_H_out will give a better indication of the stellar metallicity
       if 'Fe_H_out' not in list(data):
          data['Fe_H_out']=data['Fe_H']    
        
       return data

   
    def replace_ev_stage(self, data, iin=3, iout=2):
          ### Core Hydrogen exhaustian is better labelled as SGB iin=3, iout=2
          ### But that doesnt really make sense. Lose all diagnostics
          #from intercepts as SGB and CHE have different features caluclauted. Overruled iin=2, i out=2 
          if 'ev_stage' in list(data):
             data['ev_stage']=data['ev_stage'].replace(iin,iout)
          else: 
             data.assign(ev_stage=1)
          return data

    
    def filter_data(self,data,params):
        """Execute all the methods to filer the training data according top param file."""
        
        cols=list(data)
        exclude=[]
        exclude += self.drop_abunds(data,params)
        exclude += self.drop_ratios(data,params)
        
        if params.drop_intercepts is True:
           exclude += self.drop_incepts_slopes(data,params)    
    
        ### remove columns from the exclude list   
        pexclude=[i for i in params.exclude.split('|')]    
        dcols=[x for x in exclude+pexclude if x in cols]
        
        ### Actually drop them 
        data = data.drop(dcols, axis=1)

        ### Pick training data by evolutionary phase XXX Fix so that this is a list        
        if 0 not in params.evphase:
              data=data.loc[data['ev_stage'].isin(params.evphase)]
              print('Filtering traning data -- Keeping evphases: ', params.evphase) 
          
        ### Store all the excluded columns 
        params.ex=exclude+[a.strip() for a in params.exclude.split('|')]
 
        ### Check dictionary
        self.check_dictionary(cols)
   
        ### Store parameter limits    
        params.maxs = data.max()
        params.mins = data.min()
   
        return data 
         
          
           
    def drop_abunds(self,data,params):
       """Deal with abundances"""
       
       ### Functions to Drop Abundances
       f1 = lambda x: x+'_c'
       f2 = lambda x: x+'_surf'

       f3=lambda x: r'{}'.format(x)+'$_c$'
       f4=lambda x: r'{}'.format(x)+'$_{\\rm{surf}}$'
   
       ### Based on network include and exclude parameter lists 
       inc_network=list(filter(None, [x.strip() for x in params.inc_network.split('|')]))
       exclude = [x for x in self.nuc_network if x not in inc_network]
       exclude=[f(x) for x in exclude for f in (f1,f2)]
   
       ### Based on surface or central abundances 
       if params.drop_abund_loc==0:
         exclude += [f(x) for x in self.nuc_network for f in (f1,f2)]   
         
       elif params.drop_abund_loc==1:
         exclude += [f1(x) for x in self.nuc_network]       
         for x in self.nuc_network:
           self.y_latex_short[f2(x)]=f4(x) 
           self.y_latex[f2(x)]=f4(x)   
           
       elif params.drop_abund_loc==2:
         exclude += [f2(x) for x in self.nuc_network]
         for x in self.nuc_network:
          self.y_latex_short[f1(x)]=f4(x) 
          self.y_latex[f2(x)]=f3(x)         
        
       return exclude   
   
    
    def filter_degree(self, data):
        return [x for x in list(data) for y in self.ratios if  re.search(y+'_''.',x) ]  
   
    def drop_ratios(self,data,params):
      """Treat the ratios as desired"""
      exclude=[]  
      ### This weeds out the ratios at all radial orders. Hence the re.search of the column names
      if params.irat < 2:
         exclude+=self.filter_degree(data) 
   
      ### This just weeds out summary global ratios 
      if params.irat ==0:
         exclude +=self.ratios    
   
      ### This weeds out all instances (global, radial order, intercept) of a ratio specified in the exclude list
      if params.irat ==3:
        rrdrop=[x for x in self.ratios if x in params.exclude.split('|')]
        exclude+= [x for x in list(data) for y in rrdrop if  re.search(y+'.',x) ]      
   
      ### And we get params.irat==4 for free doing it in this order

      if params.nmx_rel is True and params.drop_intercepts is False:
         exclude=[x for x in exclude if x not in self.nmxix]   
      return exclude
  
  
    def drop_incepts_slopes (self,data,params):   
        ### Drop intercepts if asked to do so
        exclude  = [x+'c' for x in self.ratios]        
        exclude += [x+'m' for x in self.ratios]
        exclude += self.nmxix                
        return exclude 
  
    def pick_stage(self,data, iphase):
        
        if iphase==1:
            data=data.loc[data['ev_stage'].isin([1,2])]
        elif iphase ==3:
            data=data.loc[data['ev_stage'].isin([3,2])]
        return data
  
    def check_dictionary(self,cols):
       ### By now only ratios not in the y dictionary of latex labels. Lets add these. 
       ### intercept and slope are fine in plain text for now. Will be replotted for papers anyway 
       ks=self.y_latex.keys() 
       for x in cols:
          if x not in ks:
            if '_' in x:
               val=self.split_rs(x)
            else: val=x
            self.y_latex_short[x]=val
            self.y_latex[x]=val 

    def check_mon(self,x,y):
       ds=np.sign(np.diff(y))
       if len(ds) > 2:
          if ds[0] != ds[2] and ds[1] !=ds[2]:
              x=x[2:]
              y=y[2:] 
              return x,y  
          if ds[0] != ds[1] :
              x=x[1:]
              y=y[1:]  

          signs=[idx for idx, i in enumerate((np.diff(np.sign(y)) != 0)*1) if i==1]
          if 0 in signs and 1 in signs and 2 in signs:
            x=x[3:]
            y=y[3:]
            return x,y
          if 0 in signs and 1 in signs:
            x=x[2:]
            y=y[2:]
            return x,y
          if 0 in signs:   
            x=x[1:]
            y=y[1:]

       return x,y


    def split_rs(self,rat):
      parts=rat.split('_')
      return r'{}'.format(parts[0])+'$_{{}}$'.format(parts[1])


    def set_defs(self):
        """Set some defintions we will need"""
        
        self.nuc_network=['X','Y','Li','Be','B','C','N','O','F','Ne','Mg','H1','H2','He3','He4','Li7',
                          'Be7','B8','C12','C13','N13','N14','N15','O14','O15','O16','O17','O18','F17',
                          'F18','F19','Ne18','Ne19','Ne20','Ne22','Mg22','Mg24']
        self.ratios = ['r02','r01','r13','r10']

        self.xclf=['Dnu0','Teff','nu_max','Fe_H']
        
            ### Labels and some definitions 
        self.y_latex = {
             "M": r"Mass M$/$M$_\odot$", 
             "Y": r"Initial helium Y$_0$", 
             "Z": r"Initial metallicity Z$_0$", 
             "alpha": r"Mixing length $\alpha_{\mathrm{MLT}}$", 
             "diffusion": r"Diffusion factor D",
             "overshoot": r"Overshoot $\alpha_{\mathrm{ov}}$", 
             "age": r"Age $\tau/$Gyr", 
             "radius": r"Radius R$/$R$_\odot$", 
             "mass_X": r"Hydrogen mass X", 
             "mass_Y": r"Helium mass Y",
             "X_surf": r"Surface hydrogen X$_{\mathrm{surf}}$", 
             "Y_surf": r"Surface helium Y$_{\mathrm{surf}}$",
             "X_c": r"Core-hydrogen X$_{\mathrm{c}}$",
             "log_g": r"Surface gravity log g (cgs)", 
             "L": r"Luminosity L$/$L$_\odot$",
             "mass_cc": r"Convective-core mass M$_{\mathrm{cc}}$"
                }

        self.y_latex_short = {
            "M": r"M$/$M$_\odot$", 
            "Y": r"Y$_0$", 
            "Z": r"Z$_0$", 
            "alpha": r"$\alpha_{\mathrm{MLT}}$", 
            "diffusion": r"D",
            "overshoot": r"$\alpha_{\mathrm{ov}}$",
            "age": r"$\tau/$Gyr", 
            "radius": r"R$/$R$_\odot$", 
            "mass_X": r"X", 
            "mass_Y": r"Y",
            "X_surf": r"X$_{\mathrm{surf}}$", 
            "Y_surf": r"Y$_{\mathrm{surf}}$",
            "X_c": r"X$_{\mathrm{c}}$",
            "log_g": r"log g", 
            "L": r"L$/$L$_\odot$",
            "mass_cc": r"M$_{\mathrm{cc}}$"
                }     

        self.nmxix=['r01_nmxm',
                    'r01_nmxc',
                    'r10_nmxm',
                    'r10_nmxc',
                    'r02_nmxm',
                    'r01_nmxc',
                    'r13_nmxm',
                    'r13_nmxc']


    def classify(self):
       """Train a RF classifer to determine Ev stage"""
       print('Training ev_stage classifier')
       X=self.data[self.xclf]
       y=self.data['ev_stage']
       self.clf=RandomForestClassifier(n_estimators=30,n_jobs=30)
       self.clf.fit(X, y)
       

    def repalce_OS(self, data,params):
        """If the model does not have a convective core 
        then it seems silly to infer and overshoot value"""
        def overshoot2(row):
            if row['mass_cc'] > 1.e-5:
              return row['overshoot']
            else:
              return 0.0  
        
        if params.overshoot2 is True:
            data['overshoot2']=data.apply(lambda row: overshoot2(row), axis=1) 
            
        return data



    def mp_calc_newcolumns(self,data,fcols):


      """Things we are test for a might need to caclulate in post processing.
      1. Intercept and Gradient
      2. Intercept and Gradient using 5 closest modes around numax   
      3. Parabolic fit to the ratios 
      4. Evstage determined by presence of mix modes and core hydrogen abundance
      5. New overshoot and fully mixed region need to be corrected when overshoot=0.0
      """

      ### Its either both or neither corrections to OS diagnostics
      ratios=['r01','r10','r02','r13']
      tcols=['r10c', 'r10nmxc', 'r10p20', 'ev_stage2','EffOS2','PCv_1','PCr_1']
      p=[x in fcols for x in tcols]
      p.append(p[-1]) 


      message=['gradients and intercepts', 
                    'gradients and intercepts relative to nu_max',
                    'parabolic fit to ratios',
                    'Correction to evstage',
                    'Correction to new overshoot diagnostics - Case overshoot=0.0',
                    'Correction to new overshoot diagnostics - Negative OS region',
                    'Principal components of Frequencies',
                    'Principal components of Ratios'] 



      for i, boolv in enumerate(p):
         if boolv is False:
            print('Need to calculate',message[i]) 

       
      ###Need to ensure our columns here are the same order as in when we do our colworker analysis
      cols=[]
      for rat in ratios:
         if p[0] is False:
            cols.append(rat+'m') 
            cols.append(rat+'c') 
         if  p[1] is False:  
             cols.append(rat+'nmxm')
             cols.append(rat+'nmxc')
         if p[2] is False:  
             cols.append(rat+'p2a')
             cols.append(rat+'p2b')
             cols.append(rat+'p2c')  
      if p[3] is False:  
          cols.append('ev_stage2')
      if p[4] is False:  
          
              cols.append('CCB_radius')
              cols.append('CCB_mass')
              cols.append('FM_radius')
              cols.append('FM_mass')
              cols.append('OS_region_R')
              cols.append('OS_region_M')
              cols.append('Eff_OS_scale')
 
              #elif i==5:
              cols.append('EffOS2')

      #if p[6] is False:
      #    cols+= ['PCv_'+str(ii) for ii in range(1,6)]
      #    cols+= ['PCr_'+str(ii) for ii in range(1,6)]
      
      
      if any(p) is False:
        print('Will probably take a few minutes')

      cw=colworker(fcols, p)
      res=16  
      with mp.Pool(res) as pool:
         results= pool.map(cw.get_row, [row for row in data.itertuples(index=True, name='Pandas')])



      ### Now we need to collate everyhing
      results=np.array(results)

      print(results.shape)
      print(data.shape)  
      for i, col in enumerate(cols):

           data[col]=results[:,i]
     
     
      
      ### Takes longer but we will do PCA separately I think Testing and experimental only
      res=16 
      with mp.Pool(res) as pool:
         results= pool.map(cw.get_PCA, [row for row in data.itertuples(index=True, name='Pandas')])
     
     
      ###Should have returned with a big matrix of stuff 
      results=np.array(results)
      rarr=self.PCA_model(results)
      data=data.join(rarr)        

      Osgrid=data[(data['EffOS2'] > 0)]
      ids=list(set(Osgrid['id']))
      medos={}
      for idd in ids:
         medos[idd]= Osgrid[(Osgrid['id']==idd)]['EffOS2'].median()


      self.medos=medos
      medkeys=list(medos.keys())
      data['MED_EFFOS2']=data.apply(lambda row: self.medos2(row,medkeys), axis=1) 
      
      return data 


    def medos2(self, row, medkeys):
       if row['id'] in medkeys:
           return self.medos[row['id']]
       else:
           return 0.0

    def PCA_model(self,arr):     

       df=pd.DataFrame(arr)
       freqs=df.iloc[:,:27]
       rats=df.iloc[:,27:]
       
       cols=[['v9PC_1','v9PC_2','v9PC_3'],
             ['r9PC_1','r9PC_2','r9PC_3'],
             ['v5PC_1','v5PC_2','v5PC_3'],
             ['r5PC_1','r5PC_2','r5PC_3']]

       rangs=[ [i for i in range(27)],
               [i for i in range(27,54)],
               [9*i+j  for i in range(3) for j in range(5)],
               [9*i+j  for i in range(3,6) for j in range(5)] ]
       
       
       rarr=pd.DataFrame(np.nan, index=df.index, columns=[item for sublist in cols for item in sublist])
       for i, col in enumerate(cols):
           if col[0].startswith('v'):
               df1=freqs[rangs[i]]
           else:
               df1=rats[rangs[i]]

           f1=df1.dropna()    
           scaler=StandardScaler()
           f1s=scaler.fit_transform(f1) 
           pca=PCA(n_components=3)
           f1PCA=pca.fit_transform(f1s)
           f1df=pd.DataFrame(f1PCA, index=f1.index, columns=col)           
           rarr[col]=f1df.apply(lambda row: self.do_fast(row,rarr.index.values), axis=1)  
            
            
       return rarr
       
    def do_fast(self,row,indexes) :       
      if row.name in indexes:
          return row 
      else: 
          return [np.nan, np.nan, np.nan] 
      
      
class colworker(object):
   def __init__(self, fcols, p):

      self.p=p
      self.fcols=fcols
      ### Iterating through as row not as a series is faster. Bit more work here to get indexs though
      self.ratios=['r01','r10','r02','r13']
      self.nms=['nm5','nm4','nm3','nm2','nm1','n0','np1','np2','np3','np4','np5']
      self.dcols={}

      ### Column location of things we need. 
      ###The plus one here is because itertuples will insert a counter in first postion
      self.ir01 = {value:key+1 for (key, value) in enumerate(fcols) if 'r01_' in value}
      self.ir10 = {value:key+1 for (key, value) in enumerate(fcols) if 'r10_' in value}
      self.ir02 = {value:key+1 for (key, value) in enumerate(fcols) if 'r02_' in value}
      self.ir13 = {value:key+1 for (key, value) in enumerate(fcols) if 'r13_' in value}
      self.idxs = [self.ir01,self.ir10,self.ir02,self.ir13]

      self.nus={value:key+1 for (key, value) in enumerate(fcols) if value.startswith('nu_') and not value.endswith('max') }
           
      self.idnu=fcols.index('Dnu0')+1
      self.inm=fcols.index('nu_max')+1
 
      self.iev=fcols.index('ev_stage')+1  
      self.idn2=fcols.index('dnu02')+1   
      self.ixc=fcols.index('X_c')+1   
      self.ieffos=fcols.index('Eff_OS_scale')+1         

      self.iccbr=fcols.index('CCB_radius')+1
      self.iccbm=fcols.index('CCB_mass')+1
      self.ifmr=fcols.index('FM_radius')+1
      self.ifmm=fcols.index('FM_mass')+1     
      self.iosR=fcols.index('OS_region_R')+1
      self.iosM=fcols.index('OS_region_M')+1
      self.ircc=fcols.index('radius_cc')+1
      self.imcc=fcols.index('mass_cc')+1
      self.irad=fcols.index('radius')+1
      self.imass=fcols.index('M_current')+1

      self.loc0={value:key+1 for (key, value) in enumerate(self.fcols) if 'nu_0_' in value} 
      self.loc1={value:key+1 for (key, value) in enumerate(self.fcols) if 'nu_1_' in value} 
      self.loc2={value:key+1 for (key, value) in enumerate(self.fcols) if 'nu_2_' in value} 
      self.locds=[self.loc0,self.loc1,self.loc2]



   def get_row(self,row):
 
     nmx=row[self.inm]
     fwhm = (0.66 * nmx**0.88) / (2 * math.sqrt(2 * math.log(2)))
     results=[]


     for j, rat in enumerate(self.ratios):
        if any(self.p[0:3]) is False:
            ### The -1 here is because iterruples puts an counter in front of row.
            ### Indexes have been set assuming this but its not the case for accessing postions in fcols    

            ###Index of all possible orders for this ratio    
            lidx=[val for key,val in self.idxs[j].items()]    
            
            ### All possible orders for this ratio 
            orders=[int(self.fcols[k-1].split('_')[1]) for k in lidx]
            orders.sort()

            
                 
            if rat=='r13':
               nu_want=['nu_1_'+str(n) for n in orders] 
            else:
               nu_want=['nu_0_'+str(n) for n in orders]
 
 
            nu_locs=[self.nus[key] for key in nu_want]
            x=[row[k] for k in nu_locs] 
            y=[row[k] for k in lidx] 

            ###Now have frequencies in x and ratio in y. But we might have nans
            xnan=[i for i in range(len(x)) if pd.isnull(x[i])]
            ynan=[i for i in range(len(y)) if pd.isnull(y[i])]
            nans=list(set(xnan+ynan)) 
     
            x=[x[i] for i in range(len(x)) if i not in nans]   
            y=[y[i] for i in range(len(y)) if i not in nans]

            
            gaussian_env = norm.pdf(x, loc=nmx, scale=fwhm)
  
        if self.p[0] is False:
          if len(x) > 3:
             vals = self.fits(x,y,gaussian_env,1)                  
          else:
              vals=[np.nan]*2
          results+=list(vals) 
          #print(rat, 'p0', len(results), results)
          

        if self.p[1] is False:
            ### Should have frequencies and ratios that correspond and have no nans 
            ### Now get work out the index of freq closest to nuumax
            ### If we are a MS star and have ratios that is....
            if len(x)==0:
               vals =[np.nan]*2 

            else:
               i_nmx=(np.abs(np.asarray(x)-nmx)).argmin()
               n_nmx=orders[i_nmx]

               ### This should be the orders that are OK to use.  
               okords=[orders[i] for i in range(len(orders)) if i not in nans]  

               ### We cleaned this above. So it should always take a radial order unless we are at an edge
               n_want=[n_nmx-kk for kk in range(5,0,-1) if n_nmx-kk in okords]
               if  n_nmx in okords:             
                  n_want+=[n_nmx]
               n_want += [n_nmx+kk for kk in range(1,6) if n_nmx+kk in okords]
   
               if rat=='r13':
                  nu_want=['nu_1_'+str(n) for n in n_want] 
               else:
                  nu_want=['nu_0_'+str(n) for n in n_want] 

               nu_locs=[self.nus[key] for key in nu_want]

               
               x1=[row[k] for k in nu_locs] 
               y1=[row[self.idxs[j][rat+'_'+str(n)]] for n in n_want] 
                    
               ###Come back and fix. Shouldnt need to do this
               xnan1=[i for i in range(len(x1)) if pd.isnull(x1[i])]
               ynan1=[i for i in range(len(y1)) if pd.isnull(y1[i])]
               nans1=list(set(xnan1+ynan1)) 
     
               x1=[x1[i] for i in range(len(x1)) if i not in nans1]   
               y1=[y1[i] for i in range(len(y1)) if i not in nans1]
          

               gaussian_env1 = norm.pdf(x1, loc=nmx, scale=fwhm)
           
               if len(x1) > 3:
                  vals = self.fits(x1,y1,gaussian_env1,1) 

               else:
                  vals =[np.nan]*2     
            results += list(vals)
            #print(rat, 'p1', len(results), results)
            
            
        if self.p[2] is False: 
           ###Now we try fit a parabola
           if len(x) > 3:
               if rat=='r13':
                  vals=self.fits(x,y,gaussian_env1,2)
               else: 
                  vals=self.fits(x,y,gaussian_env,2)
           else:
              vals=[np.nan]*3
           results+=list(vals) 
           #print(rat, 'p2', len(results), results)

     ### Jump out of ratios loop now!!           
     if self.p[3] is False:
        if row[self.ixc] > 0.1:
           results.append(1.0)
        else:
           if np.isnan(row[self.idn2]): 
              results.append(3.0) 
           else:
              results.append(2.0)
       #print('p3', len(results), results)         

     if self.p[4] is False:  
        if row[self.ircc]  >  1e-7 and row[self.iccbr] < 1e-7:
           ### This means didnt handle OS=0.0 case properly in MESA. Lets fix it
           results.append(row[self.ircc]*row[self.irad])
           results.append(row[self.imcc]*row[self.imass])
           results.append(row[self.ircc]*row[self.irad])
           results.append(row[self.imcc]*row[self.imass])
           results.append(0.0)
           results.append(0.0)
           results.append(0.0)
        else:
           results.append(row[self.iccbr])
           results.append(row[self.iccbm])
           results.append(row[self.ifmr])
           results.append(row[self.ifmm])
           results.append(row[self.iosR])
           results.append(row[self.iosM])
           results.append(row[self.ieffos])

        ### And we always do 4 and 5 together to keep it simple
        if results[-1] <  0:
           results.append(0.0)
        else:
           results.append(results[-1])

           
     return results

   def fits(self,x,y,wts,pol):
      try:
         #slope, intercept, r_value, p_value, std_err = stats.linregress(x,y)
         fits= np.polyfit(x,y,pol,w=wts)
         return fits
      except:
        return [np.nan]*(pol+1)




   def get_PCA(self,row):
         ###Lets return nmx +- 4 orders for each el.
         ### We can then try with 9 then 7 then 5 to see how it goes
         
         mat=[]
         nmx=row[self.inm]
         
         ### Frequencies
         for el in [0,1,2]:             
           ### What frequencies are availible for this get_row
           vs={k:v for k,v in self.locds[el].items() if not pd.isnull(row[v]) and row[v] > 0 }
           ### Get the radial orders and sorted
           orders=[int(x.split('_')[-1]) for x in vs.keys()]
           orders.sort()
             
           ### Order and frequency closest to nmx
           freqs=[row[vs['nu_'+str(el)+'_'+str(x)]] for x in orders] 
           #print(orders,freqs)

           ### Do we have a zero size array?
           if len(freqs) ==0:
               mat +=[np.nan]*9
           else:
              i_nmx=(np.abs(np.asarray(freqs)-nmx)).argmin()
              n_nmx=orders[i_nmx]           
              #print(nmx, freqs[i_nmx])          
              ### need to do this so we can easily drop to three if we need be
              mat.append(freqs[i_nmx])
              for kk in range(1,5):
                 if i_nmx - kk > 0:
                    mat.append(freqs[i_nmx-kk])
                 else:    
                    mat.append(np.nan)   
                 if i_nmx+kk <= len(freqs)-1:   
                     mat.append(freqs[i_nmx+kk])
                 else:    
                    mat.append(np.nan)   
                    
         #print(mat)                       
         ### Ratios
         for j, rat in enumerate(self.ratios[:3]):
            lidx=[val for key,val in self.idxs[j].items()]    
        
            ### All possible orders for this ratio 
            orders=[int(self.fcols[k-1].split('_')[1]) for k in lidx]
            orders.sort()
            nu_want=['nu_0_'+str(n) for n in orders]
 
            #print(lidx,orders,nu_want)
            nu_locs=[self.nus[key] for key in nu_want]
            x=[row[k] for k in nu_locs] 
            y=[row[k] for k in lidx] 

            ###Now have frequencies in x and ratio in y. But we might have nans
            xnan=[i for i in range(len(x)) if pd.isnull(x[i]) or x[i] < 0]
            ynan=[i for i in range(len(y)) if pd.isnull(y[i])]
            nans=list(set(xnan+ynan)) 
     
            x=[x[i] for i in range(len(x)) if i not in nans]   
            y=[y[i] for i in range(len(y)) if i not in nans]
            #print(x,y)
            ### Do we have a zero size array?
            if len(x) ==0:
               mat +=[np.nan]*9
            else:
               i_nmx=(np.abs(np.asarray(x)-nmx)).argmin()
               mat.append(y[i_nmx])
               for kk in range(1,5):
                 if i_nmx - kk > 0:
                    mat.append(y[i_nmx-kk])
                 else:    
                    mat.append(np.nan)   
                 if i_nmx+kk <= len(y)-1:   
                     mat.append(y[i_nmx+kk])
                 else:    
                    mat.append(np.nan)                 
         #print(mat)                  
         return mat   
             

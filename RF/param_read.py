import sys
import ast
import distutils.util
import os

"""Code to use machine learning libraries to characterise stars. 
   Most of this is shamelessly ripped off from Earl's original code."""
class parameters(object):

   """Parameter file class"""
   def __init__(self, pfile):

      ### First define each of our parameters and their type
      strings = (['obs_dir',
                  'perturb_dir',
                  'out_dir',
                  'simdir',
                  'simfile',
                  'obs_pattern',
                  'freq_pattern',
                  'perturb_pattern',
                  'inc_network',
                  'excl_network',
                  'exclude'])

      ints=(['npurt',
             'lib',
             'irat',
             'random_seed',
             'drop_abund_loc',
             'ntrees',
             'obo_retry',
             'train_int_slope_all',
             'max_outside'])


      boolean =(['r_n',
                 'repickle_lims',
                 'repickle_grid',
                 'obo',
                 'train_int_slope',
                 'drop_intercepts',
                 'peak_identify',
                 'plot_kde',
                 'rf_classifier',
                 'overshoot2',
                 'nmx_rel'])

      lists =(['y_init', 'y_curr','radial_excl','radial_incl','evphase'])


      ### Read in parameter file and attribute 
      with open(pfile) as f:
         for line in f:
            #print(line) 
            if line.lstrip().startswith('#') or not line.strip():
               continue
            else:
               line = line.split('#')
               key, var = line[0].split('=')
               key = key.strip().lower()
               #print(key,var)
               if key in strings:
                   var=var.replace("'", "")
                   var=var.replace('"','')
                   var=var.strip()
                   
                   setattr(self, key, var)

               elif key in boolean:
                    setattr(self, key, 
                            bool(distutils.util.strtobool(
                                        var.strip())))
                          
               elif key in ints:
                    setattr(self, key, int(var.strip()))  

               elif key in lists:
                    setattr(self, key, ast.literal_eval(var.strip()))

               else:
                    print( "Warning: Can't find parameter in the lists of types (in get_parms)- ", key)
                    print("Stopping")
                    sys.exit(2)

      ### Test we have everything we need
      ndef=[] 
      for param in strings+ints+boolean+lists:
         try: getattr(self, param)
         except: ndef.append(param)

      if len(ndef) > 0:
         print('Error - Required Parameters not defined')
         print(ndef)  
         sys.exit(2)


      ### Various tasks
      self.fname= os.path.join(self.simdir, self.simfile)      
      self.bname=os.path.basename(self.fname).split('.')[0]
      self.picklen=os.path.join('./RF/Pickles' ,self.simfile[:-4]+'.pickle')    
      
      



      if self.train_int_slope is True and self.r_n is False:
          print('Need to calculate ratios at every radial order to use slope and intercept -- Setting r_n=True')
          self.r_n=True   
          
      if self.overshoot2 is True:
          if 'overshoot' in self.y_init:
              self.y_init.remove('overshoot')
              self.y_init.append('overshoot2')
              print ('Dropping overshoot from y_init. Replacing with overshoot2')
              
          if 'overshoot' in self.y_curr:
              self.y_curr.remove('overshoot')
              self.y_curr.append('overshoot2')
              print ('Dropping overshoot from y_curr. Replacing with overshoot2')
              

   def check_params(self):
       """Might be usefule to run some checks so that the parameters are not crazy"""

       if self.rf_classifier is True: 
          if len(self.evphase) == 1 and 0 in self.evphase:
             pass
          else:
             print('For safety we can only run the classifier when all models are present')
             print('Set evphase=[0] in params or disable classifier')
             sys.exit(1)
    
       ###Lets just drop intercepts, ratios and d02 for subgiants only...
       if len(self.evphase) == 1 and 3 in self.evphase:
           print('We are only keeping subgiants -- automatically droping ratios, slopes, intercepts, and small separation')
           self.drop_intercepts = True
           self.irat=0
           self.exclude +='|dnu02|dnu13|'


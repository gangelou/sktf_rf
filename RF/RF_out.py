from prettytable import PrettyTable
import os

import pylab as P
import corner

import matplotlib as mpl 
mpl.use("agg")
#mpl.use('Qt5Agg')
from matplotlib import pyplot as plt 
mpl.rc('font', family='serif') 
mpl.rc('text', usetex='true') 
mpl.rc('text', dvipnghack='true') 
mpl.rcParams.update({'font.size': 8}) 
import numpy as np
from scipy import stats
import peakutils

##Good luck getting this to work without importing agg

class RF_out(object):
    """Class comprising various output routines for the RF"""
    def __init__(self,star,name,X_cols,y_cols,td,params):
       self.name=name      
       self.star=star 
       self.X_cols=X_cols
       self.y_cols= y_cols
       self.y_latex=td.y_latex 
       self.y_latex_short=td.y_latex_short 
       self.params=params

    def init_table(self, sn, y_names):
         """Method to initialise table output"""
         table_curr_fname = os.path.join(self.name, 'tables', self.star+"_curr_"+sn+".dat")
         table_init_fname = os.path.join(self.name, 'tables', self.star+"_init_"+sn+".dat")
         if not os.path.exists(os.path.dirname(table_curr_fname)):
            os.makedirs(os.path.dirname(table_curr_fname))
         self.table_curr = open(table_curr_fname, 'w')
         self.table_init = open(table_init_fname, 'w')   
         self.write_table_headers(y_names, self.table_curr, self.table_init, self.params)


    def output(self, sn, predict,y_names,probs):
         """Write out the RF output"""
         cov_subdir=os.path.join(self.name, 'covs', self.star+'_'+sn+'.dat')
         out_dir=os.path.join(self.name, 'plots') 

         if not os.path.exists(os.path.dirname(cov_subdir)):
            os.makedirs(os.path.dirname(cov_subdir))
         if not os.path.exists(out_dir):
            os.makedirs(os.path.dirname(out_dir))

         np.savetxt(cov_subdir, predict,
            header=" ".join(y_names), comments='')
         
         #self.print_star(self.star, predict, y_names, self.table_curr, self.table_init, self.params)
         self.print_star2(self.star, predict, y_names, self.table_curr, self.table_init, self.params)
         self.plot_star(self.star, predict, y_names, out_dir, sn)
             
         
         print(self.x)
         self.table_curr.close()
         self.table_init.close()
         
         paper_table= os.path.join(self.name, 'tables', self.star+"_paper_"+sn+".dat") 
         self.paper_out2(self.star, predict, paper_table,self.params,y_names,probs)



    def get_rc(self,num_ys):
        rows = (num_ys+(1 if num_ys%2 else 0)) // 2
        cols = 4 if num_ys%2 else 2
        if num_ys%3==0:
            rows = num_ys//3
            cols = num_ys//rows
        sqrt = np.sqrt(num_ys)
        if int(sqrt) == sqrt:
            rows, cols = int(sqrt), int(sqrt)
        return rows, cols, sqrt

    def weighted_avg_and_std(values, weights):
        average = np.average(values, axis=0, weights=weights)
        variance = np.average((values-average)**2, axis=0, weights=weights) 
        return (average, np.sqrt(variance))

    def gumr(self,xn, xu):
        z2 = np.trunc(np.log10(xu))+1
        z1 = np.around(xu/(10**z2), 3)
        y1 = np.around(xn*10**(-z2), 2)
        value = y1*10**z2
        uncert = z1*10**z2
        return('%g'%value, '%g'%uncert)

    def print_star(self,star, predict, y_names, table_curr, table_init,params):
        middles = np.mean(predict, 0)
        stds = np.std(predict, 0)
        #middles, stds = weighted_avg_and_std(predict, 1/stds)
        outstr = star
        initstr = "\n" + star
        currstr = "\n" + star
        for (pred_j, name) in enumerate(y_names):
            (m, s) = (middles[pred_j], stds[pred_j])
            m, s = gumr(m, s)
            outstr += "\t%s pm %s" % (m, s)
            if name in params.y_init:
                initstr += r" & %s $\pm$ %s" % (m, s)
            if name in params.y_curr:
                currstr += r" & %s $\pm$ %s" % (m, s)
        print(outstr)
        table_curr.write(currstr + r' \\')
        table_init.write(initstr + r' \\')


    def write_table_headers(self,y_names, table_curr, table_init,params):
        #print("Name\t"+"\t".join(y_names))
        table_curr.write(
            r"\tablehead{\colhead{Name} & "+\
            ' & '.join([r"\colhead{" + self.y_latex_short[yy] + r"}"
                        for yy in y_names if yy in params.y_curr]) +\
            r"}\startdata" )
        table_init.write(
            r"\tablehead{\colhead{Name} & "+\
            ' & '.join([r"\colhead{" + self.y_latex_short[yy] + r"}"
                        for yy in y_names if yy in params.y_init]) +\
            r"}\startdata" )
        
    def plot_line(self,value, n, bins, plt, style):
        cands = bins > value
        if any(cands):
            height = n[np.where(bins > value)[0][0]-1]
        else:
            height = n[0]
        plt.plot((value, value), (0, height), style)

    def plot_star2(star, predict, y_names, out_dir, sn):
        
        ## Regular histograms
        middles = np.mean(predict, 0)
        stds = np.std(predict, 0)
        
        #outstr = star
        num_ys = predict.shape[1]
        
        rows, cols, sqrt = get_rc(num_ys)
        
       
        
        ### Try Preacclocation to stop mapliotlib 2.2.2 crash
        if num_ys%2==0 or num_ys%3==0 or int(sqrt)==sqrt:  
          fig,axs=plt.subplots(rows,cols,figsize=(6.97522*2, 4.17309*2), dpi=400, 
            facecolor='w', edgecolor='k', linewidth=1.)   
            
        else:    
          plt.figure(figsize=(6.97522*2, 4.17309*2), dpi=400, 
            facecolor='w', edgecolor='k', linewidth=1.)
        for (pred_j, name) in enumerate(y_names[0:num_ys]):
            (m, s) = (middles[pred_j], stds[pred_j])
            
            if num_ys%2==0 or num_ys%3==0 or int(sqrt)==sqrt:  
                ax = axs.flat[pred_j]
            elif pred_j%2==0 and pred_j == num_ys-1:
                ax = plt.subplot2grid((rows, cols), (pred_j//2, 1), colspan=2)
            else:
                ax = plt.subplot2grid((rows, cols), (pred_j//2, (pred_j%2)*2),
                    colspan=2)
            
            n, bins, patches = ax.hist(predict[:,pred_j], 50, density=1, 
                histtype='stepfilled', color='white',edgecolor='black', linewidth=1.)
            
            #if y_central is not None:
            #    mean = np.mean(y_central[:,pred_j])
            #    std = np.std(y_central[:,pred_j])
            #    
            #    plot_line(mean, n, bins, plt, 'r--')
            #    plot_line(mean+std, n, bins, plt, 'r-.')
            #    plot_line(mean-std, n, bins, plt, 'r-.')
            
            q_16, q_50, q_84 = corner.quantile(predict[:,pred_j], [0.16, 0.5, 0.84])
            q_m, q_p = q_50-q_16, q_84-q_50
            plot_line(q_50, n, bins, ax, 'k--')
            plot_line(q_16, n, bins, ax, 'k-.')
            plot_line(q_84, n, bins, ax, 'k-.')
            
  
            
            
            # Format the quantile display.
            fmt = "{{0:{0}}}".format(".3g").format
            title = r"${{{0}}}_{{-{1}}}^{{+{2}}}$"
            title = title.format(fmt(q_50), fmt(q_m), fmt(q_p))
            
            ax.annotate(r"$\epsilon = %.3g\%%$" % (s/m*100),
                xy=(0.99, 0.12), xycoords='axes fraction',
                horizontalalignment='right', verticalalignment='right')
            
            ax.set_xlabel(self.y_latex[y_names[pred_j]] + " = " + title)
            ax.locator_params(axis='x', nbins=3)
            
            xs = [max(0, m-4*s), m+4*s]
            
            xticks = [max(0, m-3*s), m, m+3*s]
            ax.set_xticks(xticks)
            ax.set_xlim(xs)
            ax.set_xticklabels(['%.3g'%xtick for xtick in xticks])
            ax.set_yticklabels('',visible=False)
            
            ax.set_frame_on(False)
            ax.get_xaxis().tick_bottom()
            ax.axes.get_yaxis().set_visible(False)
            
            #xmin, xmax = ax1.get_xaxis().get_view_interval()
            ymin, ymax = ax.get_yaxis().get_view_interval()
            ax.add_artist(mpl.lines.Line2D(xs, (ymin, ymin), 
                color='black', linewidth=2))
            
            #ax.minorticks_on()
            plt.tight_layout()

        plt.savefig(os.path.join(out_dir, star +'_'+sn+ '.pdf'), dpi=400)
        plt.close()



    def plot_star(self, star, predict, y_names, out_dir, sn):
        
        ## Regular histograms
        middles = np.mean(predict, 0)
        stds = np.std(predict, 0)
        
        #outstr = star
        num_ys = predict.shape[1]
        
        ### For outputing peak obs_data
        peaks=['# Peaks']
        
        rows, cols, sqrt = self.get_rc(num_ys)
        plt.figure(figsize=(6.97522*2, 4.17309*2), dpi=400, 
            facecolor='w', edgecolor='k', linewidth=1.)
        for (pred_j, name) in enumerate(y_names[0:num_ys]):
            (m, s) = (middles[pred_j], stds[pred_j])
            
            if num_ys%2==0 or num_ys%3==0 or int(sqrt)==sqrt:  
                ax = plt.subplot(rows, cols, pred_j+1)
            elif pred_j%2==0 and pred_j == num_ys-1:
                ax = plt.subplot2grid((rows, cols), (pred_j//2, 1), colspan=2)
            else:
                ax = plt.subplot2grid((rows, cols), (pred_j//2, (pred_j%2)*2),
                    colspan=2)
            
            n, bins, patches = ax.hist(predict[:,pred_j], 50, density=1, 
                histtype='stepfilled', color='white',edgecolor='black', linewidth=1.)
            
            #if y_central is not None:
            #    mean = np.mean(y_central[:,pred_j])
            #    std = np.std(y_central[:,pred_j])
            #    
            #    plot_line(mean, n, bins, plt, 'r--')
            #    plot_line(mean+std, n, bins, plt, 'r-.')
            #    plot_line(mean-std, n, bins, plt, 'r-.')
            
            q_16, q_50, q_84 = corner.quantile(predict[:,pred_j], [0.16, 0.5, 0.84])
            q_m, q_p = q_50-q_16, q_84-q_50
            self.plot_line(q_50, n, bins, plt, 'k--')
            self.plot_line(q_16, n, bins, plt, 'k-.')
            self.plot_line(q_84, n, bins, plt, 'k-.')
            
            ### Peak analysis
            if self.params.peak_identify is True :
              try:  
                kde = stats.gaussian_kde(predict[:,pred_j])
                kdata=kde(bins)
                indicies = peakutils.indexes(kdata, thres=0.02/max(kdata), min_dist=0.1)
                peaks.append(len(indicies))
                if self.params.plot_kde is True:
                   ax.plot(bins,kdata)
                ax.scatter(bins[indicies], kdata[indicies],c='k')
                for i, xval in enumerate(bins[indicies]):
                   ax.annotate('%0.3g'%xval, (bins[indicies][i], kdata[indicies][i]))
              except:
                  peaks.append(0) 
            
            # Format the quantile display.
            fmt = "{{0:{0}}}".format(".3g").format
            title = r"${{{0}}}_{{-{1}}}^{{+{2}}}$"
            title = title.format(fmt(q_50), fmt(q_m), fmt(q_p))
            
            ax.annotate(r"$\epsilon = %.3g\%%$" % (s/m*100),
                xy=(0.99, 0.12), xycoords='axes fraction',
                horizontalalignment='right', verticalalignment='right')
            
            P.xlabel(self.y_latex[y_names[pred_j]] + " = " + title)
            P.locator_params(axis='x', nbins=3)
            
            xs = [max(0, m-4*s), m+4*s]
            
            xticks = [max(0, m-3*s), m, m+3*s]
            ax.set_xticks(xticks)
            ax.set_xlim(xs)
            ax.set_xticklabels(['%.3g'%xtick for xtick in xticks])
            ax.set_yticklabels('',visible=False)
            
            ax.set_frame_on(False)
            ax.get_xaxis().tick_bottom()
            ax.axes.get_yaxis().set_visible(False)
            
            #xmin, xmax = ax1.get_xaxis().get_view_interval()
            ymin, ymax = ax.get_yaxis().get_view_interval()
            ax.add_artist(mpl.lines.Line2D(xs, (ymin, ymin), 
                color='black', linewidth=2))
            
            #ax.minorticks_on()
            plt.tight_layout()
        

        if self.params.peak_identify is True:
            self.x.add_row(peaks)
            
        plt.savefig(os.path.join(out_dir, star +'_'+sn+ '.pdf'), dpi=400)
        plt.close()



    def plot_importances(self, forest, star, baseout, X_names):
        """Work out the feature importances for the RF"""
        est = forest.steps[0][1]
        importances = est.feature_importances_
        indices = np.argsort(importances)[::-1]
        import_dist = np.array([tree.feature_importances_ 
            for tree in est.estimators_])
        
        cpath=os.path.join(baseout,'covs','feature-importance-'+star+'.dat')
        if not os.path.exists(os.path.dirname(cpath)):
            os.makedirs(os.path.dirname(cpath))
        
        np.savetxt(cpath,
            import_dist, header=" ".join(X_names), comments='')
        
        import_dist = import_dist.T[indices][::-1].T
        
        print("Feature ranking:")
        for f in range(len(X_names)):
            print("%d. %s (%.3g)" % (f+1, X_names[indices[f]], 
                importances[indices[f]]))
        print()
        
        mpl.rc('text', usetex='false') 
        plt.figure(figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
        plt.boxplot(import_dist, vert=0)
        plt.yticks(range(1,1+len(X_names)),
                   np.array(X_names)[indices][::-1])
        plt.xlabel("Feature importance")
        plt.tight_layout()
        cpath=os.path.join(baseout,'plots','feature-importance-'+star+'.pdf')
        if not os.path.exists(os.path.dirname(cpath)):
            os.makedirs(os.path.dirname(cpath))
        plt.savefig(cpath)
        plt.close()

        t1=['Teff','r10','r01','L','Dnu0','diffusion','Fe_H','radius','dnu02','r02']
        mlist=[]
        for val in t1:
            if val in X_names:
              idx=np.where(X_names==val)[0][0] 
              msg=' & {0:.2f}'.format(importances[idx])    
            else: 
              msg =' &'   
            mlist.append(msg)         
        out="".join(mlist)
        #print(out)
        #print(t1)


    def paper_out(star, star_data, predict, paper_table, params,ynames):
        
       """Print a table. Its format will depend on what I want to include for the paper """ 
       
       GS98logZX=np.log10(0.0230)
       t1=['Dnu0', 'Teff', 'Fe_H', 'radius', 'L', 'alpha', 'overshoot']
       t2=['M','age','X_c', 'log_g','radius', 'L', 'alpha', 'overshoot', 'Teff'] 
       
       #t2=['M','radius','age','X_c', 'log_g', 'L', 'alpha', 'overshoot','undershoot', 'mass_cc', 'cz_radius','FE_H_surf'] 
       t2=['M','radius','age','Teff','L', 'FE_H_out','alpha', 'overshoot']
       a=os.path.dirname(paper_table)
       a=os.path.dirname(a)
       obs=a.replace(params.out_dir, params.obs_dir)
       if obs.endswith('_SG'):
           obs=obs[:-3]
       fobs=[os.path.join(obs,x) for x in os.listdir(obs) if x.endswith(params.obs_pattern)] 

       for star in fobs:
          mlist=[]
          obs_data= pd.read_table(star,delim_whitespace=True)
          obs_data.columns=['Name','Value','Uncertainty']   
          obs_data.index=obs_data.Name 
          if 'Dnu0' in list(star_data):             
             obs_data.loc['Dnu0']=('Dnu0', star_data.Dnu0.mean(), star_data.Dnu0.std())
          for val in t1:
             if val in obs_data.index.values:
               if val=='Teff':
                 msg=' & ${0:d} \pm {1:d}$'.format(int(obs_data.loc[val]['Value']),int(obs_data.loc[val]['Uncertainty']))   
               else: 
                  msg=' & $ {0:.2f} \pm {1:.2f}$'.format(obs_data.loc[val]['Value'],obs_data.loc[val]['Uncertainty'])   
             
             else:
               msg =' &'  
             mlist.append(msg) 
          out="".join(mlist)
     
          print() 
          print (out)
          print()

          mlist2=[]
          middles = np.mean(predict, 0)
          stds = np.std(predict, 0)
          for val in t2:
            if val in ynames:
              idx=np.where(ynames==val)[0][0]
              msg=' & ${0:.2f} \pm {1:.2f}$'.format(middles[idx],stds[idx]) 
            elif val == 'FE_H_surf':
                idx1=np.where(ynames=='X_surf')[0][0]
                idx2=np.where(ynames=='Y_surf')[0][0] 
                Fehs= np.log10( (1.0-predict[:,idx1]-predict[:,idx2])/predict[:,idx1]) - GS98logZX
                msg=' & ${0:.2f} \pm {1:.2f}$'.format(Fehs.mean(),Fehs.std())    
       
            else: 
              msg =' &'   
            mlist2.append(msg)
          out2="".join(mlist2)
         
          print(out2)
          with open(paper_table, 'w') as wf:
            wf.writelines(out + '\n') 
            wf.writelines(out2 + '\n')


    def paper_out2(self,star, predict, paper_table, params,ynames,probs):
        GS98logZX=np.log10(0.0230)
        if params.overshoot2 is True:
           outs=['M','radius','age','L','Fe_H_out','alpha','overshoot2']
        else:
            #outs=outs=['M','radius','age','L','Fe_H_out','alpha','overshoot'] 
            outs=['M','radius','age','L','Fe_H_out']
            outs2=['mass_cc', 'radius_cc', 'MCZ2', 'RCZ2', 'alpha','overshoot']


        mlist2=[]        
        for i, parm in enumerate(outs):
          if parm in ynames:
            idx=idx=np.where(ynames==parm)[0][0]
            q_16, q_50, q_84 = corner.quantile(predict[:,idx], [0.16, 0.5, 0.84])
            q_m, q_p = q_50-q_16, q_84-q_50
            msg=' & ${0:.2f}^{{+{1:.2f}}}_{{-{2:.2f}}}$'.format(q_50, q_p,q_m) 
          else: 
            msg =' &'    
          mlist2.append(msg)
        out2="".join(mlist2)   
        print(out2)
        
        mlist2=[]        
        for i, parm in enumerate(outs2):
          if parm in ynames:
            idx=idx=np.where(ynames==parm)[0][0]
            q_16, q_50, q_84 = corner.quantile(predict[:,idx], [0.16, 0.5, 0.84])
            q_m, q_p = q_50-q_16, q_84-q_50
            if parm in ['mass_cc','radius_cc']:
               msg=' & ${0:.4f}^{{+{1:.4f}}}_{{-{2:.4f}}}$'.format(q_50, q_p,q_m) 
            else: 
               msg=' & ${0:.2f}^{{+{1:.2f}}}_{{-{2:.2f}}}$'.format(q_50, q_p,q_m) 
          else: 
            msg =' &'    
          mlist2.append(msg)
        out3="".join(mlist2)   
        print(out3)


        
        with open(paper_table, 'w') as wf:
            wf.writelines(out2 + '\n')  
            wf.writelines(out3 + '\n')  
            wf.writelines(probs)
            #if params.rf_classifier is True:
              #out3='{' + ','.join("'{}':{}".format(k, v) for k, v in self.cc.iteritems()) + '}'
              #wf.writelines(out3 + '\n') 

            
    def print_star2(self, star, predict, y_names, table_curr, table_init,params):
       middles = np.mean(predict, 0)
       stds = np.std(predict, 0)
       #middles, stds = weighted_avg_and_std(predict, 1/stds)
       outstr = star
       initstr = "\n" + star
       currstr = "\n" + star
       x = PrettyTable()
       cols=['Star']
       vals=[star]
       for (pred_j, name) in enumerate(y_names):
           (m, s) = (middles[pred_j], stds[pred_j])
           if name=='ev_stage':
              self.peak=m
           m, s = self.gumr(m, s)
           outstr = "%s pm %s" % (m, s)
           cols.append(name)
           vals.append(outstr)
           if name in params.y_init:
              initstr += r" & %s $\pm$ %s" % (m, s)
           if name in params.y_curr:
              currstr += r" & %s $\pm$ %s" % (m, s)
       #print(cols)       
       x.field_names=cols
       x.add_row(vals)
       self.x=x
       table_curr.write(currstr + r' \\')
       table_init.write(initstr + r' \\')

#!/opt/anaconda-3/anaconda3/bin/python3
# -*- coding: utf-8 -*-


import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
warnings.filterwarnings("ignore")
import matplotlib as mpl 
mpl.use("agg")
import collections
import os
from time import time
import sys
import numpy as np
import pandas as pd
import re

from sklearn.externals import joblib
from sklearn.ensemble import ExtraTreesRegressor, RandomForestRegressor
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA

from sys import argv
import tensorflow as tf
import argparse

import multiprocessing as mp
from tensorflow.contrib.learn.python import SKCompat
from sklearn.metrics import explained_variance_score
from scipy import stats
from scipy.stats import norm


import lim_perturb as lp
import RF.param_read as pr
from RF.lim_pertfreqs import  lin_reg
import RF.RF_out as RFO
from RF.RF_td import training_data


"""Random Forest Regressor to hanlde RGB Grid and Tensor Flow capabilities. 
   Tensor Flow performs very poorly compared to Scikit Learn so I stopped 
   developing it for now. Orginal code by Earl Bellinger. I have decided to put
   everything inside of classes across several source files as the code 
   was getting unweildly. Also Engineered new features and aanlyses."""    


################################################################################
### Classes ####################################################################
################################################################################
class sk_rf(object):
   """SKlearn random forest algorithm. This is where the magic happens""" 
   def __init__(self):
      self.name ='SKlearn Random Forest Regressor'
      self.sn='sk'
      self.forest=None
      self.forests=[]

   def train_regressor(self,data, X_columns, Y_columns, params):
 
       ### Get the Columns
       y_show=Y_columns
       y_show=[y for y in y_show if y not in params.ex]
       ys = data.loc[:, y_show]
       X = data.loc[:,[i for i in X_columns if i not in y_show and i in list(data)]]



       ### Now need to drop rows where there is NAN. 
       idx=X.isnull().any(axis=1)  
       kdx=[j for j in X.index if idx.loc[j]  == True]
       idx=ys.isnull().any(axis=1)    
       kdx+= [j for j in ys.index if idx.loc[j]  == True]  
       kdx=list(set(kdx))
       print('Pre Fit: Dropping train_data due to nan', len(kdx))
       #print(kdx[:20])
       X=X.drop(kdx)
       ys=ys.drop(kdx)
        
       ### Actually Train
       print()
       print('Training', self.name)
       print('Training Grid', X.shape)
       for n_trees in [params.ntrees]:
         self.forest = Pipeline(steps=[
            ('forest', ExtraTreesRegressor(
                #RandomForestRegressor(
                n_estimators=n_trees, 
                n_jobs=min(n_trees, mp.cpu_count()-2),
                oob_score=True, bootstrap=True))])
         start = time()
   
         ### Did any null values slip through 
         if X.isnull().any().any():
            print('null values in array')
            print(X.isnull().any()) 

            

         self.forest.fit(X, ys)#new_ys)
         end = time()
         print('ntree', 'oob', 'time')
         print(n_trees, self.forest.steps[0][1].oob_score_, end-start)
    
       print()
       print("%.5g seconds to train regressor" % (end-start))
       print()
    
       self.y_names = ys.columns
       self.X_names = X.columns
       

       try:
         self.stages= set(data['ev_stage'])
       except:
          print('warning not setting stages. OK for validation')  
       
   def save_forest(self):
        """Store to see if we have already trained an appropriate forest"""
        ### Not the most sophticated way of doing this
        self.forests.append((self.forest,self.X_names,self.y_names,self.stages))      
       
class tf_rf(object):
  """Class to use google's tensor flow machine learning libraries. Their random forest implementation is poor.
     It requires a lot of resources and does not perform nearly as well as scikit learn. 
     So I have stopped developing this class"""
    
  def __init__(self):
      self.name ='TensorFlow Random Forest Regressor'
      self.sn='tf'
      self.forest=None
      self.forests=[]
  def train_regressor(self,data, X_columns, params):
      
       y_show=params.y_init+ params.y_curr 
       X = data.loc[:,X_columns]
       ys = data.loc[:, [i for i in y_show if i not in X_columns]]
    
       print()
       print('Training', self.name)
 
       params = tf.contrib.tensor_forest.python.tensor_forest.ForestHParams(
                 num_classes=len(list(ys)), 
                 num_features=len(list(X)),
                 num_trees=100,
                 bagging_fraction=1.0,
                 feature_bagging_fraction=0.9,
                 regression=True).fill()


       estimator = SKCompat(tf.contrib.tensor_forest.client.random_forest.TensorForestEstimator(
                   params, model_dir="./tfout"))
       start = time()  
       if X.isnull().any().any():
            print('null values in array')
            print(X.isnull().any())  

       self.forest=estimator.fit(x=np.asarray(X).astype(np.float32), y=np.asarray(ys).astype(np.float32))
       end = time()

    
       print()
       print("%.5g seconds to train regressor" % (end-start))
       print()
       self.y_names = ys.columns
       self.X_names = X.columns

  def save_forest(self):
     ### Not the most sophticated way of doing this
     self.forests.append((self.forest,self.X_names,self.y_names))    




################################################################################
### Main Anlysis.############################### ###############################
################################################################################
class Bamboozle(object):

    def __init__(self, td, rpath, params,args):
      ### Either Run the code or perform a validation test
      if args.validate is False:
        self.process_dir(td, rpath, params)
      else:
        self.train_test(td,params)

      
    def process_dir(self, td, rpath, params):
       """Necessary steps to train our algorithm and evaluate our data. 
          I have had to break this routine up into process_dir and regress_star.
          The reason being is to do a check on subgiants. 
          Usually we assume ratios and slopes and intercepts will work but we run into trouble with SGs.
          So we will run with the features as is. Then do some checks and a second run if we think we have a subgiant"""
       
       ### Initialize and Define. 
       roots=[]
       files=[]
       stars=[]   

       train_data=td.filter_data(td.data,params)

       ### Get the directories and files. 
       for root, subdir, path in os.walk(rpath):
          if len(path) > 0:
             for p in path:
               if p.endswith(params.perturb_pattern):
                  roots.append(root)
                  files.append(os.path.basename(p).replace(params.perturb_pattern, '')) 
                  stars.append(os.path.join(root,p))

       ### Read in the star data 
       for j, star_fname in enumerate(stars):
           
           star = os.path.split(star_fname)[-1].split("_")[0]
           star_data = pd.read_csv(star_fname, sep='\t')
           lds=list(star_data)
           
           
           if 'overshoot' in lds:
               if params.overshoot2 is True:
                   star_data.rename(columns = {'overshoot':'overshoot2'}, inplace = True)
                       
           snmx=[]
           if params.nmx_rel is True:
               print('Reodering n realtive to nmx and dropping regular intercepts and gradients')
               star_data, snmx=self.nmxrel(star_data)             
                
  
           sdrop=[x for x in list(star_data) if x in params.ex]+[x for x in snmx if x in list(star_data)]            
           sd2= star_data.copy().loc[:,td.xclf]
           star_data = star_data.drop(sdrop,  axis=1).dropna()
           Ycols=params.y_init+params.y_curr

           train_data=self.set_tis(train_data, star_data, params)   
           baseout=roots[j].replace(params.perturb_dir,params.out_dir, 1)             
           RF_1  =RFO.RF_out(star,baseout, list(star_data), Ycols,td,params)
           self.probs=''           

           ### Always good to run the classifier first 
           if params.rf_classifier is True:
                 if 'ev_stage' not in RF_1.y_cols:
                    RF_1.y_cols.append('ev_stage')   
                     
                 iphase=self.RF_classify(sd2,td,RF_1)
                 X_cols = self.get_Xcols(list(star_data),td,iphase,RF_1.X_cols)
                 self.regress_star(RF_1, star_data.loc[:,X_cols],td.pick_stage(train_data, iphase),params)
              
                  
           else:
              self.regress_star(RF_1, star_data.loc[:,RF_1.X_cols],train_data,params)
           
           
           
    def load_default_forest(self, params,train_data,Xcols,Ycols):
        """Here we have a default forest to help deal with subgiants and ratios/intercepts"""     
        for reg in regs:
           print(reg.name, '-- Training default forest') 
           reg.train_regressor(train_data, Xcols,Ycols,params)
           reg.save_forest()
               
    def regress_star(self, star, star_data, train_data, params):  

           ### Send to appropriate regressor library. Do some intialization. 
           for reg in regs: 

             wrong_cols = []
             outside = []
             run_times = []
                      
             ### Check we don't already have an appropriate forest. Gets tricky now with slopes and intercepts
             Train = True
             if len(reg.forests) >  0:
                 for fst in reg.forests:
                     sdc=[x for x in star_data.columns if x not in star.y_cols]
                     if set(sdc) ==set(fst[1]) and set(train_data['ev_stage'])==fst[3] :
                         
                       ### Always retrain if we calculate slopes and intercepts from specified/limited modes  
                       if params.train_int_slope is True and params.train_int_slope_all==1:  
                         print('We are training on slopes and intercepts. Furthermore, train_int_slope_all=1.')
                         print('This means we redo the linear regression based on the availible modes of each star')
                         print('Not worth checking this. Retraining instead')
                         Train=True
                         
                       ### If we are using all slopes and intercepts, we comapre and hope for the best. 
                       ### If we dont use slopes/intercepts can also use appropriate forest.    
                       elif (params.train_int_slope is True and params.train_int_slope_all==0) or  params.train_int_slope is False: 
                         print('Already trained an appropriate forest, Loading')
                         reg.forest=fst[0]
                         reg.X_names=fst[1]
                         reg.y_names=fst[2]
                         reg.stages=fst[3] 
                         Train=False
                         
             ### Train Regressor if need be. Must use list(star_data) here not star.X_cols  
             if reg.forest is None or Train is True :
                print('Training New RF for ',star.star) 
                reg.train_regressor(train_data, list(star_data),star.y_cols,params)
                reg.save_forest()

             ### Assign local variables
             forest=reg.forest
             X_names=reg.X_names
             y_names=reg.y_names

             star.init_table(reg.sn,y_names)                      
             star.plot_importances(forest, star.star+'-'+reg.sn, star.name, X_names)

             ### Redundancy 1: check that it has all of the right columns. Followed by messy checks
             if not set(X_names).issubset(set(star_data.columns)):
                wrong_cols += [star]
                continue

             star_data =self.redundancy_check(X_names,star_data, params)           
             print("Instantiations after range check", star_data.shape)
                           
             ### Send star data to Random Forest
             star_X = star_data.loc[:,X_names]
             start = time()
             predict = forest.predict(star_X)

             ### Save and Print Results
             end = time()
             run_times += [end-start]
             star.output(reg.sn,predict,y_names,self.probs)
             
             
             ### Diagnostics
             print("\ntotal prediction time:", sum(run_times))
             print("time per star:", np.mean(run_times), "+/-", np.std(run_times))
             print("time per perturbation:", np.mean(np.array(run_times) / 10000), "+/-",
             np.std(np.array(run_times) / 10000))
             print("\ncouldn't process", wrong_cols)
             print("out of bounds", outside)


    def set_tis(self,  train_data, star_data, params):
           """Set train intercept and slope calculations"""
            
            
           ### Need to post-process data based on what we do to slope and intercept:
           ### Everything we want to drop is already done in data. 
           ### train_int_slope is used by the perturb code too. Hence its under perturb params in infile
           if params.train_int_slope is True :
               #print(list(data))
               if not any( x in list(train_data) for x in  ['r02c','r01c','r10c','r13c'] ):
                   print('Slopes and intercepts not present in training data. Assumed they have been dropped')
               
                   
               if params.train_int_slope_all==1:
                   print('Requesting that the slopes and intercepts are calculated from those modes availible in the observational data.')
                   print('Recaclulating but not pickling')        
                   train_data=td.calc_inct2(train_data, list(star_data))
           return train_data


    def nmxrel(self,star_data):
         """Can I just pass this to the one I use for the training data"""
         star_data= td.calc_nmxgrid(star_data)
         ratios=['r01','r10','r02','r13']
         mxc=[x+'c' for x in ratios] +  [x+'m' for x in ratios]
         return star_data,mxc  

    def get_Xcols(self, cols ,td, phase, R1X_cols):
           """Check the evolutionary phase from the default forest"""
           
           ### If we have the intercepts we need to check if its a subgiant with default forest
                 ### If its a subgiant we drop intercepts and train only on subgiants
                 ### If its a MS we drop subgiants and train on MS  with slopes and intercepts
           ### If not using slopes and intercepts ensure ev_stage. 
           ### Train as is and let user decide after what to do with evstage

           if phase==3:
                Xexclude= [x for x in cols for y in td.ratios if  re.search(y+'.',x) ]
                Xexclude += [x for x in cols for y in td.ratios if  re.search(y+'_''.',x)  ]
                Xexclude += [x for x in cols if x  in td.ratios]
                Xexclude.append('dnu02')
                X_cols=[x for x in cols if x not in Xexclude]
                                            
           elif phase ==1:
                X_cols=R1X_cols
           
           return  X_cols 


    def RF_classify(self, sd2,td,RF_1):
       """Run the star through the RF Classifier"""

       if all(x in list(sd2) for x in td.xclf):
           probs=''
           evdict={1:'MS', 2:'TO', 3:'SG'} 
           results=td.clf.predict(sd2)
           counts=collections.Counter(results)
           total=sum([val for key, val in counts.items()]) 
           for key, val in counts.items():
              msg='{0:s}={1:0.4f}, '.format(evdict[key],val/total)
              probs +=msg
               
           print('RF Classifier Probabilities: ', probs)
           self.probs=probs                
       else: 
           miss=[x for x in td.xclf if x not in list(sd2)]
           print('Require', td.xclf, 'for ev_stage RFclassifier')
           print('Missing', miss) 
         
         
       for key in evdict.keys():
           if key not in counts.keys():
               counts[key]=0
               
       if counts[1] > counts[3]:
           iphase=1
       else:
           iphase=3

       print('RF evphase', iphase,'=', evdict[iphase])
       print('Training with CHE and', evdict[iphase])
       
       #RF_1.cc=counts
       return iphase


    def redundancy_check(self,X_names, star_data, params):
             """PErform a couple of redundancy checks on the star data"""
                            
             ### Redundancy 2: check if all the params are within the grid
             ### Probably shouldnt happen.
             out_of_range = False
             
             idx=[]
             oname=[]
             
             for X_name in set(X_names):
                upper = params.maxs[X_name]
                lower = params.mins[X_name]
                X_vals = star_data.loc[:, X_name]
                if np.any(X_vals > upper) or np.any(X_vals < lower):
                   
                   t1=list(np.where(X_vals > upper)[0])
                   t2=list(np.where(X_vals < lower)[0])
                   oname+= [(X_name, 'upper')]*len(t1)
                   oname+= [(X_name, 'lower')]*len(t1)
                   #print(X_name, t1, t2) 
                   idx += t1+t2
             
             
             inds=star_data.index
             idx=[inds[x] for x in idx]
             ### Can drop a few instantiations if outside the grid params:
             if params.max_outside > 0:
                if len(idx) < params.max_outside:             
                  star_data=star_data.drop(idx)
                else:
                 print('Too many instantiations outside grid range')
                 #print(oname)
                 #for kk in range(len(idx)):
                 #   print(oname[kk], idx[kk])
                 return star_data   
                 quit()
             else:
               
               star_data=star_data.drop(idx)
               

             return star_data




    def train_test(self, td, params, MS=True):
      """Here we drop some tracks from our grid and see how well our RF predicts the 
         missing data. We use an explained_variance_score to get an initial indication. 
         MS=true is for the MS grid and drops models appropriately from there. 
         It was unclear to me at the time of writing whether we need special treatment 
         for different phases, hence the MS flag.""" 
      
      if MS is True:
        data=td.filter_data(td.data,params)  
        ### Something Earl needs probably for inversions.  
        if '03' in list(data):  
          data=data.drop(['O3','O1','O2','FF'],axis=1) 
          
        ### Get the Sun observational data  
        sundat=pd.read_table('RF/Validate/SunTest_1/sun-freqs/sun-freqs_perturb.dat')
        sundat=sundat.drop(['nu_max'],axis=1)
        
        
        ### Slice sizes for breaking up training data 
        for skip in [16,8,4,2,1]:
          div=32/skip
          print('Dropping every',skip, 'models per track')  
                      
          ###Validation on MS Grid
          Tdata=data.iloc[::][::skip]
          #Tdata.reset_index() 
          train_idx=[]
          dev_idx=[]


          ### We have dropped models from the tracks, but we need to pull out entire tracks as well.       
          for jj in range(len(Tdata)):
             n=jj//div
             if n%2==0:
              train_idx.append(jj)
             else: dev_idx.append(jj)
             
          ### These are want our inputs and predictions are    
          X_columns = ['Teff', 'Fe_H', 'Dnu0', 'dnu02', 'r02', 'r01', 'dnu13', 'r13', 'r10']
          Y_columns= ['M', 'Y', 'Z', 'alpha', 'overshoot', 'diffusion', 'age', 'X_c', 'mass_X', 'mass_Y', 'X_surf', 'Y_surf', 'radius', 'L']
          params.y_init=Y_columns
          params.y_curr=[]
          data_train=Tdata.iloc[train_idx][X_columns+Y_columns]
          #print(data_train)
          X_dev, Y_dev=Tdata.iloc[dev_idx][X_columns],Tdata.iloc[dev_idx][Y_columns]

          ### Now need to drop rows where there is NAN. 
          idx=X_dev.isnull().any(axis=1)  
          kdx=[j for j in X_dev.index if idx.loc[j]  == True]
          idx=Y_dev.isnull().any(axis=1)    
          kdx+= [j for j in Y_dev.index if idx.loc[j]  == True]  
          kdx=list(set(kdx))
          X_dev=X_dev.drop(kdx)
          Y_dev=Y_dev.drop(kdx)

            
          ### regs is the regressor can compare SKlearn and TensorFlow is so inclined
          for reg in params.regs:
           #reg.train_regressor(data_train, X_columns,y_show=Y_columns)
           reg.train_regressor(data_train, X_columns,Y_columns,params)
           star_X = X_dev.loc[:,X_columns]
           #print(star_X) 
           Xpred=reg.forest.predict(star_X)
           star_X = sundat.loc[:,X_columns]
           Spred=reg.forest.predict(star_X)
           #print(Xpred)
           if reg.sn=='tf':
               Xpred=Xpred['scores']


           ### Explained Variance score for the listed parameters             
           for y in ['M','Z','age','radius','L','diffusion']:
              idx= Y_columns.index(y)
              score=explained_variance_score(Y_dev[y],Xpred[:,idx])
              print(reg.sn,':', y, 'Explained Variance',score)
              
           ### Predictions for the sun    
           for y in ['M','Z','age','radius','L','diffusion']:
              idx= Y_columns.index(y)
              smean=Spred[:,idx].mean()
              sstd=Spred[:,idx].std()
              print(reg.sn,':', y, 'Sun pred: ',r'{:0.3f} pm {:0.3f}'.format(smean,sstd))



        ### Special case where we use all the training data 
        data_train=data[X_columns+Y_columns]
        for reg in params.regs:
           reg.train_regressor(data_train, X_columns,Y_columns,params)
           star_X = sundat.loc[:,X_columns]
           Spred=reg.forest.predict(star_X)
           print(Spred.shape)
           print('full data')
           for y in ['M','Z','age','radius','L','diffusion']:
              idx= Y_columns.index(y)
              smean=Spred[:,idx].mean()
              sstd=Spred[:,idx].std()
              print(reg.sn,':', y, 'Sun pred: ',r'{:0.3f} pm {:0.3f}'.format(smean,sstd))
           

     

if __name__ == "__main__":
   #################################################################################
   ### Start; Assumes all files to be processed are inside a 'star' directory    ###
   ### This directory is in data and/or perturb.                                 ###
   ### -p flag will create perturbed files from the observational data           ###
   ### -v will split data into test and dev for validation not input obs req     ###
   #################################################################################

   ### Parse args
   parser = argparse.ArgumentParser(description='Adding tensorflow capability to Bellinger et al. (2016) SKlearn RF')
   parser.add_argument(dest="parentd", type=str,
                    help="Directory Name in $data_dir or $perturb_dir in which to process")

   parser.add_argument('-o', dest="parmfile", type=str, default="./RF/RF_in.parms",
                    help="Location of RF parmaterfile")


   parser.add_argument("-p", "--perturb",action="store_true", default=False,
                    help="(re)perturb the data")

   parser.add_argument("-v", "--validate",action="store_true", default=False,
                    help="Split into training and dev set and report")

   args = parser.parse_args()



   params=pr.parameters(args.parmfile)

   ### for reproducibility
   np.random.seed(seed=params.random_seed) 

   ### Perturb data if required
   if args.perturb is True:
     lp.run_perturb(args.parentd, params)
   
   ### RF Regressor(s) to use
   if params.lib==0 or params.lib > 2:
     skrf=sk_rf()
     tfrf=tf_rf()
     regs=[skrf, tfrf]
   elif params.lib==1:
      skrf=sk_rf()
      regs=[skrf]
   elif params.lib ==2:
      tfrf=tf_rf()
      regs=[tfrf]

   params.regs=regs
   params.check_params()

   ### Load grid of models 
   td = training_data(params)
   rpath=os.path.join(params.perturb_dir, args.parentd)

   ### Optional classifier to evaluate phase
   if params.rf_classifier is True:
     td.classify()

   ### Run the Forest
   Bamboozle(td, rpath, params, args)
